var Graph = Graph || {};

Graph.Node = function(id, type, scores, links, data, graph){
    /// <summary>
    /// This is the graph node class
    /// </summary>
    
    this.graph = graph;
    this.id = id;
    this.type = type;
    this.scores = scores;
    if(links != undefined) this.links = links 
    else this.links = {};
    this.data = data;
}

Graph.Node.prototype.addLink = function(link, targetId){
    this.links[targetId] = link;
};

Graph.Node.prototype.removeLink = function(targetId){
    delete this.links[targetId];
}

Graph.Node.prototype.addDocument = function(doc){
    this.documents.push(doc);
};

Graph.Node.prototype.getLink = function(node){
    return this.links[node.id];
}

Graph.Link = function(source, target, scores, data, graph, id){
    /// <summary>
    /// This is the graph link class
    /// </summary>
    
    this.graph = graph;
    this.source = source;
    this.target = target;
    this.scores = scores;
    this.data = data;
    this.id = id;
}

Graph.Link.prototype.addDocument = function(doc){
    this.documents.push(doc);
}

Graph.Graph = function(nodes, links, id){
    /// <summary>
    /// This is the graph class
    /// </summary>
    
    this.id = id;
    if(nodes != undefined) this.nodes = nodes;
    else this.nodes = {};
    if(links != undefined) this.links = links;
    else this.links = {};
    this.topLinks ={};
    this.topNodes = {};
    this.selectedNodeScore = {};
    this.selectedLinkScore = undefined;
    this.linkScoreScale = d3.scale.linear()
                            .domain([0,1])
                            .range([0,1]);
    this.nodeScoreScale = d3.scale.linear()
                            .domain([0,1])
                            .range([0,1]);
    this.info = {}; //for additional data
}

Graph.Graph.prototype.clearGraph = function(){
    this.clearNodes();
    this.clearLinks();
    this.topLinks = {};
    this.topNodes = {};
    this.info = {};
}

Graph.Graph.prototype.clearNodes = function(){
    this.nodes = {};
};

Graph.Graph.prototype.clearLinks = function(){
    this.links = {};
};


//TODO Not here but in application
Graph.Graph.prototype.updateStatistics = function(){
    for(n in this.nodes){
        var node = this.nodes[n];
        if(Object.keys(node.links).length >= Object.keys(this.mostConnectedNode.links).length) this.mostConnectedNode = node;
        if(node.totalScore >= this.highestTotalScoreNode.totalScore) this.highestTotalScoreNode = node;
        if(node.averageScore >= this.highestAverageScoreNode.averageScore) this.highestAverageScoreNode = node;
    }
    for(l in this.links){
        var link = this.links[l];
        if(link.totalStrength >= this.totalStrongestLink.totalStrength) this.totalStrongestLink = link;
        if(link.averageStrength >= this.averageStrongestLink.averageStrength) this. averageStrongestLink = link;
    }
}

Graph.Graph.prototype.addNode = function(id, type, scores, links, data){
    var newNode = new Graph.Node(id, type, scores, links, data, this);
    this.nodes[id] = newNode;
    return newNode;
}

Graph.Graph.prototype.removeNode = function(node){
    for(l in node.links){
        var link = node.links[l];
        this.removeLink(link);
    }
    delete this.nodes[node.id];
}

Graph.Graph.prototype.addLink = function(source, target, scores, data){
    var newLink = new Graph.Link(source, target, scores, data, this, source.id + target.id);
    this.links[source.id + target.id] = newLink;
    source.addLink(newLink, target.id);
    target.addLink(newLink, source.id);
    return newLink;
}

Graph.Graph.prototype.removeLink = function(link){
    link.source.removeLink(link.target.id);
    if(Object.keys(link.source.links).length == 0) this.removeNode(link.source);
    link.target.removeLink(link.source.id);
    if(Object.keys(link.target.links).length == 0) this.removeNode(link.target);
    if(this.links[link.source.id + link.target.id] != undefined) delete this.links[link.source.id + link.target.id];
    if(this.links[link.target.id + link.source.id] != undefined) delete this.links[link.target.id + link.source.id]
}

Graph.Graph.prototype.getNodeById = function(id){
    return this.nodes[id];
}

Graph.Graph.prototype.getLinkBySourceTarget = function(source, target){
    if(this.links[source.id + target.id] != undefined) return this.links[source.id + target.id];
    if(this.links[target.id + source.id] != undefined) return this.links[target.id + source.id];
    return undefined;
}