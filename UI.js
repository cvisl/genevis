UI = {};

var windowWidth = window.innerWidth-4,
    windowHeight = window.innerHeight-4;

document.body.style.overflow = "hidden";

UI.clusterSVG = d3.select("#clusterSVG")
    .attr("width", windowWidth)
    .attr("height", windowHeight)
    .on("click", function(){
        UI.deactivateGeneQueryMode;
    })
    .on("dblclick", function(){UI.focusPrimary();});

UI.geneSVG = d3.select("#geneSVG")
    .attr("width", windowWidth)
    .attr("height", windowHeight);

UI.parameterPanel = d3.select("#parameterPanel");
UI.infoPanel = d3.select("#infoPanel");
UI.introPanel = d3.select("#introPanel")
    .style("width", windowWidth/2 + "px");

UI.introPanelSVG = d3.select("#introPanelSVG");
UI.primaryParameterSVG = d3.select("#primaryParameterSVG");
UI.infoPanelSVG = d3.select("#infoPanelSVG");

UI.geneHighlightButton = d3.select("#geneHighlightButton")
UI.dynamicHighlightExpansionButton = d3.select("#dynamicHighlightExpansionButton");
UI.textDisableButton = d3.select("#textDisableButton");
UI.diseaseButton = d3.select("#diseaseButton");
UI.helpButton = d3.select("#helpButton");
UI.clusterBar = d3.select("#clusterBar");
UI.interactionBar = d3.select("#interactionBar");
UI.diseaseBar = d3.select("#diseaseBar");

UI.clusterSVG.node().zoomMappingX = d3.scale.linear()
    .domain([0, UI.clusterSVG.attr("width")])
    .range([0, UI.clusterSVG.attr("width")]);
        
UI.clusterSVG.node().zoomMappingY = d3.scale.linear()
    .domain([0, UI.clusterSVG.attr("height")])
    .range([0, UI.clusterSVG.attr("height")]);
    
UI.geneSVG.node().zoomMappingX = d3.scale.linear()
    .domain([0, UI.geneSVG.attr("width")])
    .range([0, UI.geneSVG.attr("width")]);
        
UI.geneSVG.node().zoomMappingY = d3.scale.linear()
    .domain([0, UI.geneSVG.attr("height")])
    .range([0, UI.geneSVG.attr("height")]);

UI.clusterSVG.node().zoomState = {x:0,y:0,k:1};
UI.geneSVG.node().zoomState = {x:0,y:0,k:1};
    
UI.init = function(){ 
    UI.primaryParameterSVG.select("#foreignObject")
        .attr("width", windowWidth)
        .attr("height", windowHeight);

    UI.infoPanelSVG.select("#foreignObject")
        .attr("width", windowWidth)
        .attr("height", windowHeight);

    UI.introPanelSVG.select("#foreignObject")
        .attr("width", windowWidth)
        .attr("height", windowHeight);

    var introPanelWidth = parseInt(UI.introPanel.style("width")) + (parseInt(UI.introPanel.style("padding-top"))*2);
    var introPanelHeight = parseInt(UI.introPanel.style("height")) + (parseInt(UI.introPanel.style("padding-top"))*2);
    UI.introPanelSVG.style("width", introPanelWidth +"px");
    UI.introPanelSVG.style("height", introPanelHeight +"px");
    UI.introPanelSVG.style("left", (windowWidth/2)-(introPanelWidth/2) +"px");
    UI.introPanelSVG.style("top", (windowHeight/2)-(introPanelHeight/2) +"px");
    UI.movePanel(UI.introPanelSVG.select(".closeButton"),  0, introPanelWidth - 50,  50, 50, undefined, 0)

    UI.primaryParameterSVG.style("width", parseInt(UI.parameterPanel.style("width")) + (parseInt(UI.parameterPanel.style("padding-top"))*2) +"px");
    UI.primaryParameterSVG.style("height", parseInt(UI.parameterPanel.style("height")) + (parseInt(UI.parameterPanel.style("padding-top"))*2) +"px");

    //Mousewheel Events
    UI.parameterPanel.select("#linkStrength").onmousewheel = UI.onmousewheel;
    UI.parameterPanel.select("#charge").onmousewheel = UI.onmousewheel;
    UI.parameterPanel.select("#friction").onmousewheel = UI.onmousewheel;
    UI.parameterPanel.select("#theta").onmousewheel = UI.onmousewheel;
    UI.parameterPanel.select("#clusterAssociationThreshold").onmousewheel = UI.onmousewheel;
    UI.parameterPanel.select("#highlightThreshold").onmousewheel = UI.onmousewheel;
    UI.parameterPanel.select("#highlightTop").onmousewheel = UI.onmousewheel;
    UI.parameterPanel.select("#highlightLevelOfConnection").onmousewheel = UI.onmousewheel;

    geneGraph.info.highlightMode = UI.parameterPanel.select("#geneHighLightMode").node().value;
    UI.parameterPanel.select("#highlightThreshold").node().disabled = geneGraph.info.highlightMode != "threshold";
    UI.parameterPanel.select("#highlightTop").node().disabled = geneGraph.info.highlightMode != "top";
    UI.parameterPanel.select("#highlightLevelOfConnection").node().disabled = geneGraph.info.highlightMode != "lvlOfConnection";

    UI.clusterSVG.node().zoomMapper = d3.behavior.zoom()
            .x(UI.clusterSVG.property("zoomMappingX"))
            .y(UI.clusterSVG.property("zoomMappingY"))
            .on("zoom", UI.svgRedraw)

    var zoomOverlayPrimary = UI.clusterSVG
        .insert("g", ":first-child")
        .call(UI.clusterSVG.node().zoomMapper)
            .on("dblclick.zoom", null);

    UI.clusterSVG.node().zoomOverlay = zoomOverlayPrimary.append("rect")       //Zoom overlay
        .attr("class", "overlay")
        .attr("width", UI.clusterSVG.attr("width"))
        .attr("height", UI.clusterSVG.attr("height"));

    UI.clusterSVG.node().drawPanel = zoomOverlayPrimary.append("g");
    
    UI.geneSVG.node().zoomMapper = d3.behavior.zoom()
            .x(UI.geneSVG.property("zoomMappingX"))
            .y(UI.geneSVG.property("zoomMappingY"))
            .on("zoom", UI.svgRedraw);
    
    var zoomOverlaySecondary = UI.geneSVG
        .insert("g", ":first-child")
        .call(UI.geneSVG.node().zoomMapper)
            .on("dblclick.zoom", null);

    UI.geneSVG.node().zoomOverlay = zoomOverlaySecondary.append("rect")       //Zoom overlay
        .attr("class", "overlay")
        .attr("width", UI.geneSVG.attr("width"))
        .attr("height", UI.geneSVG.attr("height"));

    UI.geneSVG.node().drawPanel =  zoomOverlaySecondary.append("g");

    window.onresize();

    UI.hidePanel(UI.infoPanelSVG.node());
    UI.hidePanel(UI.primaryParameterSVG.node());

    UI.introPanel.select(".noClusterColumn").style("display" , "");         
    UI.introPanel.selectAll(".clusterColumnSelectionCell").style("display", "none");
    UI.introPanel.select(".noEdgeScoreColumn").style("display" , "");         
    UI.introPanel.selectAll(".edgeScoreColumnSelectionCell").style("display", "none");
    UI.introPanel.select(".noDiseaseColumn").style("display" , "");         
    UI.introPanel.selectAll(".diseaseColumnSelectionCell").style("display", "none");
    UI.introPanel.selectAll("#edgeDataLoaderTable").style("display", "none");
    UI.introPanel.selectAll("#diseaseDataLoaderTable").style("display", "none");
    UI.resizePanelOnContent(UI.introPanel);
};

window.onresize = function(event){
    windowWidth = window.innerWidth-4;
    windowHeight = window.innerHeight-4;

    if(UI.geneSVG.style("display") == "none"){
        UI.clusterSVG
            .attr("width", windowWidth)
            .attr("height", windowHeight)
            .style("width", windowWidth +"px")
            .style("height", windowHeight +"px");

        UI.geneSVG
            .attr("width", 0)
            .attr("height", windowHeight)
            .style("width", 0 +"px")
            .style("height", windowHeight +"px")
            .style("top", 0 +"px")
            .style("left", windowWidth +"px");

            clusterGraph.visualState.currentForceLayout.size([windowWidth, windowHeight]);
            subGeneGraph.visualState.currentForceLayout.size([windowWidth, windowHeight]);
    } 
    else{
        UI.clusterSVG
            .attr("width", (windowWidth - (windowWidth*2/3))-1)
            .attr("height", windowHeight)
            .style("width", (windowWidth - (windowWidth*2/3))-1 +"px")
            .style("height", windowHeight +"px");

        UI.geneSVG
            .attr("width", windowWidth*2/3)
            .attr("height", windowHeight)
            .style("width", windowWidth*2/3 +"px")
            .style("height", windowHeight +"px")
            .style("top", 0 +"px")
            .style("left", (windowWidth - (windowWidth*2/3))+1 +"px");

        clusterGraph.visualState.currentForceLayout.size([ (windowWidth - (windowWidth*2/3))-1, windowHeight]);
        subGeneGraph.visualState.currentForceLayout.size([windowWidth*2/3, windowHeight]);
    }

    var infoPanelWidth = parseInt(UI.infoPanel.style("width")) + (parseInt(UI.infoPanel.style("padding-top"))*2);
    var infoPanelHeight = parseInt(UI.infoPanel.style("height")) + (parseInt(UI.infoPanel.style("padding-top"))*2);
    UI.infoPanelSVG.style("width", infoPanelWidth +"px");
    UI.infoPanelSVG.style("height", infoPanelHeight +"px");
    UI.infoPanelSVG.style("left", windowWidth-infoPanelWidth +"px");

    UI.clusterBar.style("top", (windowHeight-parseInt(UI.clusterBar.style("height"))) +"px");
    UI.clusterBar.style("left", 0 +"px");
    UI.clusterBar.style("width", (windowWidth/3-1) + "px");

    UI.interactionBar.style("top", (windowHeight-parseInt(UI.interactionBar.style("height"))) +"px");
    UI.interactionBar.style("left", (windowWidth/3+1) +"px");
    UI.interactionBar.style("width", (windowWidth/3-102) + "px");

    UI.diseaseBar.style("top", (windowHeight-parseInt(UI.diseaseBar.style("height"))) +"px");
    UI.diseaseBar.style("left", (((windowWidth/3)*2)-99) +"px");
    UI.diseaseBar.style("width", ((windowWidth/3 + 100)-1) + "px");

    UI.helpButton.style("top",  (windowHeight-((parseInt(UI.diseaseBar.style("height")) + parseInt(UI.helpButton.style("height")))+2)) +"px");
    UI.helpButton.style("left", (windowWidth-50) +"px");
}

UI.updateStatistics = function(){
    UI.parameterPanel.select("#numberOfPrimaryNodes").text(Object.keys(clusterGraph.nodes).length);
    UI.parameterPanel.select("#numberOfPrimaryLinks").text(Object.keys(clusterGraph.links).length);
    UI.parameterPanel.select("#numberOfSecondaryNodes").text(Object.keys(subGeneGraph.nodes).length);
    UI.parameterPanel.select("#numberOfSecondaryLinks").text(Object.keys(subGeneGraph.links).length);
}
        
UI.initParameters = function(){
    //FDG parameters
    /*UI.parameterPanel.select("#linkStrength").node().value = 0.01;
    UI.parameterPanel.select("#charge").node().value = typeof svg.property("force").charge() === "function" ? NaN : svg.property("force").charge();
    UI.parameterPanel.select("#gravity").node().value = svg.property("force").gravity();
    UI.parameterPanel.select("#friction").node().value = svg.property("force").friction();
    UI.parameterPanel.select("#theta").node().value = svg.property("force").theta();*/
    UI.parameterPanel.select("#clusterAssociationThreshold").node().value = clusterGraph.info.clusterAssociationTreshold;
    UI.parameterPanel.select("#highlightThreshold").node().value = geneGraph.info.highlightThreshold;
    UI.parameterPanel.select("#highlightTop").node().value = geneGraph.info.highlightTop;
    UI.parameterPanel.select("#highlightLevelOfConnection").node().value = geneGraph.info.highlightConnectionLevel;
}

UI.updateGeneQueryLists = function(){
    UI.updateGeneQueryList($("#panelGeneQuery"), "#parameterPanel");
    UI.updateGeneQueryList($("#buttonGeneQuery"), null);
}

UI.updateGeneQueryList = function(geneQueryElement, parentId){
        var availableTags = Object.keys(geneGraph.nodes).filter(function(d){return geneGraph.nodes[d].data.clusters.length != 0;});
    geneQueryElement.autocomplete({
        source: function(request, response) {
            var results = $.ui.autocomplete.filter(availableTags, request.term);

            response(results.slice(0, 10));
        },
        //source: availableTags,
        select: function( event, ui ){

            geneGraph.info.currentQueriedGene = ui.item.value.toUpperCase();
            UI.activateGeneQueryMode();
        },
        position: {collision: "flipfit"},
        appendTo: parentId
    });
}

UI.movePanel = function(panel, top, left, width, height, endCallback, duration){
    duration = duration == undefined ? 500 : duration;
  return panel
          .transition()
            .duration(duration)
            .attr("width", width)
            .attr("height", height)
            .attr("x", left)
            .attr("y", top)
            .style("width", width +"px")
            .style("height", height +"px")
            .style("top", top +"px")
            .style("left", left +"px")
            .each("end", endCallback)   
}

UI.moveSVGElement = function(panel, top, left, width, height, endCallback, duration){
    duration = duration == undefined ? 500 : duration;
  return panel
          .transition()
            .duration(duration)
            .attr("width", width)
            .attr("height", height)
            .attr("x", left)
            .attr("y", top)
            .style("width", width +"px")
            .style("height", height +"px")
            .style("top", top +"px")
            .style("left", left +"px")
            .each("end", endCallback)   
}

UI.moveSVGPanel = function(svgPanel, top, left, width, height, scale, endCallback){
    UI.movePanel(svgPanel, top, left, width, height, endCallback);
    var tran = UI.movePanel(svgPanel.node().zoomOverlay, 0, 0, width, height, endCallback)
        .tween("zoomTween", function(){
            var xi = d3.interpolateNumber(this.farthestViewportElement.zoomState.x, width/2-width/(2*scale));
            var yi = d3.interpolateNumber(this.farthestViewportElement.zoomState.y, height/2-height/(2*scale));
            var si = d3.interpolateNumber(this.farthestViewportElement.zoomState.k, 1/scale);
            return function(t){
                svgPanel.node().zoomMapper.translate([xi(t),yi(t)]).scale(si(t)).event(svgPanel.select("g"));
            };
        })
}

UI.zoomContentAutoFit = function(svgPanel) {
    UI.manualZoom(svgPanel, 1/(svgPanel.node().zoomState.k*(svgPanel.attr("width")/svgPanel.node().drawPanel.node().getBBox().width)))
}

UI.manualZoom = function(svgPanel, scale){
    var zoomOverlay = svgPanel.node().zoomOverlay;
    var width = zoomOverlay.attr("width");
    var height = zoomOverlay.attr("height");
    zoomOverlay
        .transition()
            .duration(500)
            .tween("zoomTween", function(){
                var xi = d3.interpolateNumber(this.farthestViewportElement.zoomState.x, width/2-width/(2*scale));
                var yi = d3.interpolateNumber(this.farthestViewportElement.zoomState.y, height/2-height/(2*scale));
                var si = d3.interpolateNumber(this.farthestViewportElement.zoomState.k, 1/scale);
                return function(t){
                    svgPanel.node().zoomMapper.translate([xi(t),yi(t)]).scale(si(t)).event(svgPanel.select("g"));
                };
            })
}

UI.resizePanelOnContent = function(panel){
    var maxwidth = 0;
    var panelelement = panel.node();
    for(var i = 0; i < panelelement.children.length; i++){
        var currentWidth = parseInt(d3.select(panelelement.children[i]).style("width")); 
        maxwidth = currentWidth > maxwidth ? currentWidth : maxwidth;
    }

    var panelWidth = maxwidth + (parseInt(panel.style("padding-right"))*2);
    var panelHeight = parseInt(panel.style("height")) + (parseInt(panel.style("padding-top"))*2);
    var corner = panel.node().parentElement.parentElement.getAttribute("corner");
    d3.select(panel.node().parentElement.parentElement)
        .transition()
            .duration(500)
            .style("width" , panelWidth +"px")
            .style("height" , panelHeight +"px")
            .style("left",  corner == null ? d3.select(panel.node().parentElement.parentElement).style("left") : corner == "left" ? windowWidth-panelWidth +"px" : corner == "right" ? 0 +"px" : (windowWidth/2)-(panelWidth/2) + "px");
}

UI.focusPrimary = function(){
  UI.moveSVGPanel(UI.clusterSVG,     0, 0, windowWidth, windowHeight, 1);
  UI.moveSVGPanel(UI.geneSVG, 0, windowWidth, 0, windowHeight, 8, endTransfunction); 

  function endTransfunction(){
    UI.geneSVG.node().resetVisuals();

    //clusterGraph.visualState.currentForceLayout.startIf();
    subGeneGraph.visualState.currentForceLayout.size([windowWidth/2, windowHeight]).stop();
    
    UI.geneSVG.style("display", "none");
  }
}

UI.focusSecondary = function(){
  UI.moveSVGPanel(UI.clusterSVG, 0, 0, (windowWidth - (windowWidth*2/3))-1, windowHeight, 1);
  UI.moveSVGPanel(UI.geneSVG, 0, (windowWidth - (windowWidth*2/3))+1, windowWidth*2/3, windowHeight, 8, endTransfunction);  

  function endTransfunction(){
    clusterGraph.visualState.currentForceLayout.stop();
    subGeneGraph.visualState.currentForceLayout.size([windowWidth*2/3, windowHeight]).startIf();

    UI.manualZoom(UI.clusterSVG,8);
    UI.geneSVG.style("display", "");
    updateGraphVisuals(subGeneGraph, UI.geneSVG, geneNodeAppender, geneLinkAppender, geneLinkUpdater);
        //UI.zoomContentAutoFit(UI.clusterSVG);
    //UI.manualZoom(UI.clusterSVG, 1/(UI.clusterSVG.node().zoomState.k*(UI.clusterSVG.attr("width")/UI.clusterSVG.node().drawPanel.node().getBBox().width)));
    //UI.manualZoom(UI.geneSVG, 1/(UI.geneSVG.node().zoomState.k*(UI.geneSVG.attr("width")/UI.geneSVG.node().drawPanel.node().getBBox().width)));
  }
}

UI.svgRedraw = function(){
    //var graph = UI.clusterSVG.select(".node").data()[0].graph;
    //if(graph.visualState.currentForceLayout.alpha() < 0.05 || graph.id != "clusterGraph"){
        d3.select(this).selectAll(".node")
          .attr("transform", function(d){
            return "translate(" + this.farthestViewportElement.zoomMappingX(d.x) + "," + this.farthestViewportElement.zoomMappingY(d.y) + ")";
          });
        
        d3.select(this).selectAll(".link")
          .attr("x1", function(d) { return this.farthestViewportElement.zoomMappingX(d.source.x); })
          .attr("y1", function(d) { return this.farthestViewportElement.zoomMappingY(d.source.y); })
          .attr("x2", function(d) { return this.farthestViewportElement.zoomMappingX(d.target.x); })
          .attr("y2", function(d) { return this.farthestViewportElement.zoomMappingY(d.target.y); });
    //}
    this.farthestViewportElement.zoomState.x = d3.event.translate[0];
    this.farthestViewportElement.zoomState.y = d3.event.translate[1];
    this.farthestViewportElement.zoomState.k = d3.event.scale;
    d3.select("#zoomstatex").text(d3.event.translate[0]);
    d3.select("#zoomstatey").text(d3.event.translate[1]);
    d3.select("#zoomstatescale").text(d3.event.scale);
};

// Parameter Panel settings
UI.onmousewheel = function(event){
    if(!this.disabled){
        if (event.wheelDelta > 0) {
            if((parseFloat(this.value) + parseFloat(this.step)) <= this.max) this.value = parseFloat(this.value) + parseFloat(this.step);
            else this.value = this.max;
        } else {
            if((parseFloat(this.value) - parseFloat(this.step)) >= this.min) this.value = parseFloat(this.value) - parseFloat(this.step);
            else this.value = this.min;
        }
        if(this.oninput != undefined) this.oninput();
    }
    return false;
};

UI.updateInfoPanel = function(gene){
    var geneData = geneGraph.nodes[gene.id];
    UI.infoPanel.style("display", "inline-block");
    UI.infoPanel.select("#geneInfoPanelTitle").text(geneData.id + " gene");
    UI.infoPanel.select("#GWASstudy").attr("href", "http://www.gwascentral.org/generegion/phenotypes?q="+geneData.id+"&t=ZERO&vt=all&l=asd&page=1&format=html");
    UI.infoPanel.select("#GWASmarkers").attr("href", "http://www.gwascentral.org/generegion/markers?q="+geneData.id+"&t=ZERO&m=all&page=1&format=html");
    UI.infoPanel.select("#HIPPIEdataset").attr("href", "http://cbdm.mdc-berlin.de/tools/hippie/query.php?s="+geneData.id);
    UI.infoPanel.select("#NCBI").attr("href", "http://www.ncbi.nlm.nih.gov/gene?cmd=Retrieve&dopt=summary&list_uids="+geneData.data.geneEntrezId);

    var currentUserDataTable = UI.infoPanel.select("#userDataTableBody");
    var newUserDataTable = document.createElement("tbody");
    newUserDataTable.setAttribute("id","userDataTableBody");
    Object.keys(geneData.data).forEach(function(d){
        var value = geneData.data[d];
        var tableRow = document.createElement("tr");
        var keyCell = document.createElement("td");
        var valueCell = document.createElement("td");
        keyCell.textContent = d;
        if(d == "clusters"){
            value.forEach(function(d){
                valueCell.innerHTML += d.cluster + ", " + "<br>";
            });
            valueCell.innerHTML = valueCell.innerHTML.substring(0, valueCell.innerHTML.length - 2);
        }
        else if(d == "traits"){
            Object.keys(value).forEach(function(disease){
                var subRow = document.createElement("tr");
                var subKeyCell = document.createElement("td");
                var subValueCell = document.createElement("td");
                subKeyCell.textContent = disease;
                subValueCell.textContent = value[disease];
                subRow.appendChild(subKeyCell);
                subRow.appendChild(subValueCell);
                valueCell.appendChild(subRow);
            })
        } 
        else valueCell.textContent = value;
        tableRow.appendChild(keyCell);
        tableRow.appendChild(valueCell);
        newUserDataTable.appendChild(tableRow);
    })
    currentUserDataTable.node().parentElement.replaceChild(newUserDataTable, currentUserDataTable.node());

    var panelSVG = UI.infoPanelSVG[0][0];
     var panelContent = d3.select(panelSVG).select(".panelContent");
    UI.enlargePanel(panelSVG,panelContent);
}

d3.selectAll(".enlargeButton").on("click", function(){
    var panelSVG = this.farthestViewportElement;
    var panelContent = d3.select(panelSVG).select(".panelContent");
    UI.toggleEnlargePanel(panelSVG,panelContent);
});

d3.selectAll("#parameterPanel").on("mouseleave", function(){
    d3.event.stopPropagation();
    UI.hidePanel(this.parentElement.farthestViewportElement);
})

// d3.selectAll("#infoPanelEnlargeButton").on("click", function(){
//     d3.event.stopPropagation();
//     UI.hidePanel(this.parentElement);
// })

UI.toggleEnlargePanel = function(panelSVG, panelContent){

    if(panelSVG.getAttribute("enlarged") == "true"){
        UI.hidePanel(panelSVG);
    } 
    else{
        UI.enlargePanel(panelSVG, panelContent);
    }
}

UI.enlargePanel = function(panelSVG, panelContent){
    if(panelSVG.getAttribute("corner") == "left") UI.movePanel(d3.select(panelSVG), 
                                                0, 
                                                0, 
                                                parseInt(panelContent.style("width")) + (parseInt(panelContent.style("padding-top"))*2),
                                                parseInt(panelContent.style("height")) + (parseInt(panelContent.style("padding-top"))*2), 
                                                undefined);
    if(panelSVG.getAttribute("corner") == "right"){
        UI.movePanel(d3.select(panelSVG), 
            0, 
            windowWidth-parseInt(panelContent.style("width")) - (parseInt(panelContent.style("padding-top"))*2), 
            parseInt(panelContent.style("width")) + (parseInt(panelContent.style("padding-top"))*2),
            parseInt(panelContent.style("height")) + (parseInt(panelContent.style("padding-top"))*2), 
            undefined);    
        UI.movePanel(d3.select(panelSVG).select(".enlargeButton"),  0, parseInt(panelContent.style("width")) + (parseInt(panelContent.style("padding-top"))*2) - 50,  50, 50, undefined);
    } 
    panelSVG.setAttribute("enlarged", true);
}

UI.hidePanel = function(panelSVG){
    if(panelSVG.getAttribute("corner") == "left") UI.movePanel(d3.select(panelSVG), 0, 0, 50, 50, undefined);  
    if(panelSVG.getAttribute("corner") == "right"){
       UI.movePanel(d3.select(panelSVG), 0, windowWidth-50, 50, 50, undefined);  
       UI.movePanel(d3.select(panelSVG).select(".enlargeButton"),  0, windowWidth-50, 50, 50, undefined);
       UI.movePanel(d3.select(panelSVG).select(".enlargeButton"),  0, 0,  50, 50, undefined);
    } 
    panelSVG.setAttribute("enlarged", false);   
}

UI.parameterPanel.select("#linkStrength").on("input", function(){
    var linkStr = UI.parameterPanel.select("#linkStrength").node().value;
    clusterGraph.visualState.currentForceLayout.linkStrength(linkStr).startIf();
});

UI.parameterPanel.select("#charge").on("input", function(){
    var ch = UI.parameterPanel.select("#charge").node().value;
    clusterGraph.visualState.currentForceLayout.charge(ch).startIf();
});

UI.parameterPanel.select("#gravity").on("input", function(){
    var gr = UI.parameterPanel.select("#gravity").node().value;
    clusterGraph.visualState.currentForceLayout.gravity(gr).startIf()
});

UI.parameterPanel.select("#friction").on("input", function(){
    var fr = UI.parameterPanel.select("#friction").node().value;
    clusterGraph.visualState.currentForceLayout.friction(fr).startIf()
});

UI.parameterPanel.select("#theta").on("input", function(){
    var t = UI.parameterPanel.select("#theta").node().value;    
    clusterGraph.visualState.currentForceLayout.theta(t).startIf()
});

UI.parameterPanel.select("#dynamicLinkStrength").on("click", function(){
    if(this.checked){
        UI.parameterPanel.select("#linkStrength").property("disabled", true);
        UI.parameterPanel.select("#linkStrength").property("readOnly", true);
        clusterGraph.visualState.currentForceLayout.linkStrength(UI.clusterSVG.node().dynamicLinkStrength).startIf();
        subGeneGraph.visualState.currentForceLayout.linkStrength(UI.geneSVG.node().dynamicLinkStrength).startIf();
    }
    else 
    {
        UI.parameterPanel.select("#linkStrength").property("disabled", false);
        UI.parameterPanel.select("#linkStrength").property("readOnly", false);
        clusterGraph.visualState.currentForceLayout.linkStrength(UI.clusterSVG.node().defaultLinkStrength).startIf();
        clusterGraph.visualState.currentForceLayout.linkStrength(UI.geneSVG.node().defaultLinkStrength).startIf();
    }
});

UI.parameterPanel.select("#clusterAssociationThreshold").on("change", function(){
    clusterGraph.info.clusterAssociationTreshold = UI.parameterPanel.select("#clusterAssociationThreshold").node().value;
    resetScene();
});

UI.parameterPanel.select("#highlightThreshold").on("input", function(){
    geneGraph.info.highlightThreshold = UI.parameterPanel.select("#highlightThreshold").node().value;
    geneQuery();
});

UI.parameterPanel.select("#highlightTop").on("input", function(){
    geneGraph.info.highlightTop = UI.parameterPanel.select("#highlightTop").node().value;
    geneQuery();
});

UI.parameterPanel.select("#highlightLevelOfConnection").on("input", function(){
    geneGraph.info.highlightConnectionLevel = UI.parameterPanel.select("#highlightLevelOfConnection").node().value;
    geneQuery();
});

d3.selectAll(".geneQuery").on("click", function(){
    this.value = "";
});

d3.selectAll(".geneQuery").on("keyup", function(){
    if(d3.event.which == 13){
            geneGraph.info.currentQueriedGene = this.value.toUpperCase();
        UI.activateGeneQueryMode();
    }
});

UI.parameterPanel.select("#searchQuery").on("click", function(){
    geneGraph.info.currentQueriedGene = UI.parameterPanel.select(".geneQuery").property("value").toUpperCase();
    UI.activateGeneQueryMode();
});

UI.parameterPanel.select("#geneEdgeScoreSelection").on("change", function(){
    subGeneGraph.selectedLinkScore = this.value;
    subGeneGraph.linkScoreScale.domain(geneGraph.info.edgeScoresMaxMins[this.value]);
    updateGraphVisuals(subGeneGraph, UI.geneSVG, geneNodeAppender, geneLinkAppender, geneLinkUpdater);
    //if(geneGraph.info.currentQueriedGene != undefined) geneQuery(geneGraph.info.currentQueriedGene, true);
});

d3.selectAll(".diseaseSelection").on("change", function(){
    d3.event.stopImmediatePropagation()
    clusterGraph.selectedNodeScore.color = this.value;
    subGeneGraph.selectedNodeScore.color = this.value;
    States.maps.clusterDiseaseNodeColorMap.domain(getMinMaxNodes(clusterGraph, ["data","traits", clusterGraph.selectedNodeScore.color,"EASE"])); //TEST THIS IS NORMALIZED
    //States.maps.clusterDiseaseNodeColorMap.domain([0,1]);
    clusterNodeUpdater(UI.clusterSVG.selectAll(".node"));
    geneNodeUpdater(UI.geneSVG.selectAll(".node"));
});

UI.updateSelections = function(name, newItems){
    selectionLists = d3.selectAll("#"+name+"Selection");
    
    selectionLists[0].forEach(function(selectionList){
        while(selectionList.lastChild){
            selectionList.removeChild(selectionList.lastChild);
        }
        
        newItems.sort();
        
        newItems.forEach(function(item){
            var option = document.createElement("option");
           option.text = item;
           option.value = item;
           selectionList.add(option); 
        });
    })
}

UI.parameterPanel.select("#geneHighLightMode").on("change", function(){
    geneGraph.info.highlightMode = UI.parameterPanel.select("#geneHighLightMode").node().value;
    UI.parameterPanel.select("#highlightThreshold").node().disabled = geneGraph.info.highlightMode != "threshold";
    UI.parameterPanel.select("#highlightTop").node().disabled = geneGraph.info.highlightMode != "top";
    UI.parameterPanel.select("#highlightLevelOfConnection").node().disabled = geneGraph.info.highlightMode != "lvlOfConnection";
    geneQuery();
})

UI.parameterPanel.select("#cancelQuery").on("click", function(){
    UI.deactivateGeneQueryMode();
})

UI.parameterPanel.select("#diseaseMode").on("click", function(){
    if(this.checked){
        UI.activateDiseaseMode();  
            UI.resizePanelOnContent(UI.parameterPanel);     
    }
    else{
        UI.deactivateDiseaseMode();
            UI.resizePanelOnContent(UI.parameterPanel);
    }
})

UI.diseaseButton.on("click", function(){
    if(UI.diseaseButton.classed("button off")){
        UI.activateDiseaseMode();
    }
    else{
        UI.deactivateDiseaseMode();
    }
})

UI.geneHighlightButton.on("click", function(){
        UI.activateGeneQueryMode();
})

UI.dynamicHighlightExpansionButton.on("click", function(){
    if(UI.dynamicHighlightExpansionButton.classed("button off")){
        UI.activateDynamicHighlightExpansion();
    }
    else{
        UI.deactivateDynamicHighlightExpansion();
    }
})

UI.textDisableButton.on("click", function(){
    if(UI.textDisableButton.classed("button off")){
       d3.selectAll(".clusterText,.geneText").classed("disabled", false);
       UI.textDisableButton.classed({'button': true, 'on':true, 'off':false});
       UI.textDisableButton.select(".buttonText").text("Labels ON");
    } 
    else{
        d3.selectAll(".clusterText,.geneText").classed("disabled", true);
        UI.textDisableButton.classed({'button': true, 'on':false, 'off':true});
        UI.textDisableButton.select(".buttonText").text("Labels OFF");
    } 
})

UI.helpButton.on("click", function(){
        UI.introPanelSVG.style("display", "");
        UI.resizePanelOnContent(UI.introPanel);
})

UI.activateGeneQueryMode = function(){
    if(UI.geneHighlightButton.classed("button off") || 
        (geneGraph.info.prevQueriedGene != geneGraph.info.currentQueriedGene)){
            var queriedGene = geneGraph.nodes[geneGraph.info.currentQueriedGene];
            UI.geneHighlightButton.classed({'button': true, 'on':true, 'off':false});
            UI.geneHighlightButton.select(".buttonText").text("Query ON");
            if(queriedGene!=undefined){
                d3.selectAll(".geneQuery").property("value", queriedGene.id); 
                UI.updateInfoPanel(queriedGene); 
            } 
            geneQuery();
            geneGraph.info.prevQueriedGene = geneGraph.info.currentQueriedGene;
    }
    else{
        UI.deactivateGeneQueryMode();
    }

}

UI.deactivateGeneQueryMode = function(){
    UI.geneHighlightButton.classed({'button': true, 'on':false, 'off':true});
    UI.geneHighlightButton.select(".buttonText").text("Query OFF");
    UI.hidePanel(UI.infoPanelSVG.node());
    geneQuery();
}

UI.activateDiseaseMode = function(){
    UI.parameterPanel.select("#diseaseMode").node().checked = true;
    UI.diseaseButton.classed({'button': true, 'on':true, 'off':false});
    UI.diseaseButton.select(".buttonText").text("Disease mode ON");
    States.maps.clusterDiseaseNodeColorMap.domain(getMinMaxNodes(clusterGraph, ["data","traits",clusterGraph.selectedNodeScore.color, "EASE"]));
    //States.maps.clusterDiseaseNodeColorMap.domain([0,1]);
    clusterGraph.visualState.setCurrentVisualState("disease");
    subGeneGraph.visualState.setCurrentVisualState("disease");
    clusterNodeUpdater(UI.clusterSVG.selectAll(".node"));
    geneNodeUpdater(UI.geneSVG.selectAll(".node"));
}

UI.deactivateDiseaseMode = function(){
    UI.parameterPanel.select("#diseaseMode").node().checked = false;
    UI.diseaseButton.classed({'button': true, 'on':false, 'off':true});
    UI.diseaseButton.select(".buttonText").text("Disease mode OFF");
    States.maps.clusterDiseaseNodeColorMap.domain(getMinMaxNodes(clusterGraph, ["data","traits", clusterGraph.selectedNodeScore.color, "EASE"]));
    //States.maps.clusterDiseaseNodeColorMap.domain([0,1]);
    clusterGraph.visualState.setCurrentVisualState("default");
    subGeneGraph.visualState.setCurrentVisualState("default");
    clusterNodeUpdater(UI.clusterSVG.selectAll(".node"));
    geneNodeUpdater(UI.geneSVG.selectAll(".node"));
}

UI.parameterPanel.select("#highlightExpandMode").on("click", function(){
    if(this.checked){
        UI.activateDynamicHighlightExpansion();
    }
    else{
        UI.deactivateDynamicHighlightExpansion();
    }
})

UI.activateDynamicHighlightExpansion = function(){
    UI.dynamicHighlightExpansionButton.classed({'button': true, 'on':true, 'off':false});
    UI.dynamicHighlightExpansionButton.select(".buttonText").text("Dynamic highlight expansion ON");
    UI.parameterPanel.select("#highlightExpandMode").node().checked  = true;
    birdNestView();
    geneGraph.info.expandChecked = true;
}

UI.deactivateDynamicHighlightExpansion = function(){
    UI.dynamicHighlightExpansionButton.classed({'button': true, 'on':false, 'off':true});
    UI.dynamicHighlightExpansionButton.select(".buttonText").text("Dynamic highlight expansion OFF");
    UI.parameterPanel.select("#highlightExpandMode").node().checked  = false;
    unbirdNestView();
    geneGraph.info.expandChecked = false;
}

UI.parameterPanel.select("#freezeForce").on("click", function(){
        freezeForceLayout(clusterGraph, this.checked);
        freezeForceLayout(subGeneGraph, this.checked);
})

UI.introPanel.select("#clusterDataLoader").on("change", function(){
    var dataFile = this.files[0];
    var dataFileUrl = window.URL.createObjectURL(dataFile);

    var clusterDataHandler = new DataHandlers.ClusterDataHandler(dataFileUrl, "tsv", function(){
        UI.introPanel.select(".noClusterColumn").style("display" , "none");
        UI.introPanel.selectAll(".clusterColumnSelectionCell").style("display", "");
        var predictedGeneCell = "";
        var predictedIdCell = "";
        this.dataHeader.forEach(function(h){
            var geneCellOption = document.createElement("option");
            geneCellOption.text = h;
            var entrezIdCellOption = document.createElement("option");
            entrezIdCellOption.text = h;
            var clusterCellOption = document.createElement("option");
            clusterCellOption.text = h;
            UI.introPanel.select("#geneCellSelection").node().add(geneCellOption);
            UI.introPanel.select("#entrezidCellSelection").node().add(entrezIdCellOption);
            UI.introPanel.select("#clusterCellSelection").node().add(clusterCellOption);
            if(h.match(/gene.*name/i) != null) predictedGeneCell = h;
            if(h.match(/gene.*id/i) != null ) predictedIdCell = h;
        });
        UI.introPanel.select("#geneCellSelection").node().value = predictedGeneCell;
        UI.introPanel.select("#entrezidCellSelection").node().value = predictedIdCell;
        UI.resizePanelOnContent(UI.introPanel);
        UI.introPanel.select("#processLoadedClusterData").on("click", function(){
            UI.introPanel.select(".noClusterScoreColumn").style("display", "none");
            UI.introPanel.select(".clusterprogressbar").style("display", "");
            this.columnsOfImportance.gene = UI.introPanel.select("#geneCellSelection").node().value;
            this.columnsOfImportance.entrezid = UI.introPanel.select("#entrezidCellSelection").node().value;
            this.columnsOfImportance.clusters = [].slice.call(UI.introPanel.select("#clusterCellSelection").node().selectedOptions).map(function(d){return d.value;});
            this.processData();
            clusterGraph.selectedLinkScore = "jacard";
            clusterGraph.selectedNodeScore.scale = { "ellipseSizeScore":"nrOfGenes","ellipseAxisScore":"averageAssociation" };
            UI.updateStatistics();
           updateAllVisuals();
            UI.introPanel.selectAll("#edgeDataLoaderTable").style("display", "");
            UI.introPanel.selectAll("#diseaseDataLoaderTable").style("display", "");
            UI.resizePanelOnContent(UI.introPanel);
        }.bind(this));
    },function(percentage){
            UI.introPanel.select(".clusterprogressbar").select("progress").node().value = percentage*100;
        });
})

UI.introPanel.select("#edgeDataLoader").on("change", function(){
    var dataFile = this.files[0];
    var dataFileUrl = window.URL.createObjectURL(dataFile);

    var edgeScoreDataHandler = new DataHandlers.EdgeDataHandler(dataFileUrl, "tsv", function(){
        var predictedSourceCell = "";
        var predictedTargetCell = "";
        this.dataHeader.forEach(function(h){
            var option1 = document.createElement("option");
            option1.text = h;
            var option2 = document.createElement("option");
            option2.text = h;
            var option3 = document.createElement("option");
            option3.text = h;
            UI.introPanel.select("#edgeScoreSourceSelection").node().add(option1);
            UI.introPanel.select("#edgeScoreTargetSelection").node().add(option2);
            UI.introPanel.select("#edgeScoreEdgeScoresSelection").node().add(option3);
            if(h.match(/source.*name/i) != null) predictedSourceCell = h;
            if(h.match(/target.*name/i) != null) predictedTargetCell = h;
        });
        UI.introPanel.select("#edgeScoreSourceSelection").node().value = predictedSourceCell;
        UI.introPanel.select("#edgeScoreTargetSelection").node().value = predictedTargetCell;
        UI.resizePanelOnContent(UI.introPanel);
        UI.introPanel.select("#processLoadedEdgeData").on("click", function(){
            UI.introPanel.select(".noEdgeScoreColumn").style("display" , "none");
            UI.introPanel.selectAll(".edgeScoreColumnSelectionCell").style("display", "");
            this.columnsOfImportance.source = UI.introPanel.select("#edgeScoreSourceSelection").node().value;
            this.columnsOfImportance.target = UI.introPanel.select("#edgeScoreTargetSelection").node().value;
            this.columnsOfImportance.scores = [].slice.call(UI.introPanel.select("#edgeScoreEdgeScoresSelection").node().selectedOptions).map(function(d){return d.value;});
            this.processData();
            subGeneGraph.selectedLinkScore = geneGraph.selectedLinkScore;
            UI.parameterPanel.select("#geneEdgeScoreSelection").node().value = this.columnsOfImportance.scores[0];
            UI.updateSelections("geneEdgeScore", this.columnsOfImportance.scores); 
            UI.updateStatistics();
            if(subGeneGraph.id != undefined) buildSubGeneGraph(clusterGraph.nodes[subGeneGraph.id]);
            updateAllVisuals();
            UI.resizePanelOnContent(UI.introPanel);
        }.bind(this));
    },function(percentage){
            UI.introPanel.select(".edgeprogressbar").select("progress").node().value = percentage*100;
        });
})

UI.introPanel.select("#diseaseDataLoader").on("change", function(){
    var dataFile = this.files[0];
    var dataFileUrl = window.URL.createObjectURL(dataFile);

    var traitDataHandler = new DataHandlers.DiseaseDataHandler(dataFileUrl, "tsv", function(){
        var predictedGeneCell = "";
        var predictedTraitCell = "";
        var predictedScoreCell = "";
        this.dataHeader.forEach(function(h){
            var option1 = document.createElement("option");
            option1.text = h;
            var option2 = document.createElement("option");
            option2.text = h;
            var option3 = document.createElement("option");
            option3.text = h;
            UI.introPanel.select("#geneColumnSelection").node().add(option1);
            UI.introPanel.select("#traitColumnSelection").node().add(option2);
            UI.introPanel.select("#scoreColumnSelection").node().add(option3);
            if(h.match(/gene/i) != null) predictedGeneCell = h;
            if(h.match(/disease|trait|function|cluster/i) != null) predictedTraitCell = h;
            if(h.match(/score|p.*value/i) != null) predictedScoreCell = h;
        });
        UI.introPanel.select("#geneColumnSelection").node().value = predictedGeneCell;
        UI.introPanel.select("#traitColumnSelection").node().value = predictedTraitCell;
        UI.introPanel.select("#scoreColumnSelection").node().value = predictedScoreCell;
        UI.introPanel.select(".noDiseaseColumn").style("display" , "none");
        UI.introPanel.selectAll(".diseaseColumnSelectionCell").style("display", "");
        UI.resizePanelOnContent(UI.introPanel);
        UI.introPanel.select("#processLoadedDiseaseData").on("click", function(){
            UI.introPanel.select(".noDiseaseScoreColumn").style("display", "none");
            UI.introPanel.select(".diseaseprogressbar").style("display", "");
            this.columnsOfImportance.gene = [UI.introPanel.select("#geneColumnSelection").node().value];
            this.columnsOfImportance.trait = UI.introPanel.select("#traitColumnSelection").node().value;
            this.columnsOfImportance.score = UI.introPanel.select("#scoreColumnSelection").node().value;
            this.processData();
            UI.updateSelections("diseaseBarDisease", Object.keys(diseaseSets));
            clusterGraph.selectedNodeScore.color = Object.keys(diseaseSets)[0];
            subGeneGraph.selectedNodeScore.color = Object.keys(diseaseSets)[0];
            UI.updateStatistics();
            updateAllVisuals();
            UI.resizePanelOnContent(UI.introPanel);
        }.bind(this));
    }, function(percentage){
            UI.introPanel.select(".diseaseprogressbar").select("progress").node().value = percentage*100;
        });
})

UI.introPanelSVG.select("#introPanelCloseButton").on("click",function(){
    UI.introPanelSVG.style("display", "none");
})

UI.clusterBar.select("#defaultClusterButton").on("click", function(){
    var dataFileUrl;    
    switch(UI.clusterBar.select("#defaultClusterSelect").node().value){
        case "Reactome":
            dataFileUrl = "/data/Defaultsets/cluster/reactomeparsed.tsv";
            break;
        case "KEGG":
            dataFileUrl = "/data/Defaultsets/cluster/KEGGparsed.tsv";
            break;
        default:
            dataFileUrl = "/data/Defaultsets/cluster/KEGGparsed.tsv";
    }
    //UI.introPanel.select(".noClusterColumn").style("display", "none");
    //UI.introPanel.select(".clusterprogressbar").style("display", "");

    var clusterDataHandler = new DataHandlers.ClusterDataHandler(dataFileUrl, "tsv", function(){
        this.columnsOfImportance.gene = "geneName";
        this.columnsOfImportance.entrezid = "geneEntrezId";
        this.columnsOfImportance.clusters = this.dataHeader;
        this.columnsOfImportance.clusters.splice(this.columnsOfImportance.clusters.indexOf("geneName"),1);
        this.columnsOfImportance.clusters.splice(this.columnsOfImportance.clusters.indexOf("geneEntrezId"),1);
        this.processData();
        //UI.introPanel.select(".noClusterColumn").style("display", "");
        //UI.introPanel.select(".clusterprogressbar").style("display", "none");
        //UI.introPanel.select(".noClusterColumn").text("Default cluster set chosen, no column selection");
        //UI.introPanel.select(".noEdgeScoreColumn").text("No edge score data file loaded...");
        //UI.introPanel.select(".noDiseaseColumn").text("No disease/function data file loaded...");
        //UI.introPanel.select(".noClusterColumn").attr("colspan", 2);
        UI.resizePanelOnContent(UI.introPanel);
        clusterGraph.selectedLinkScore = "jacard";
        clusterGraph.selectedNodeScore.scale = { "ellipseSizeScore":"nrOfGenes","ellipseAxisScore":"averageAssociation" };
        UI.updateStatistics();
        updateAllVisuals();
        UI.manualZoom(UI.clusterSVG,4);
        //UI.introPanel.selectAll("#edgeDataLoaderTable").style("display", "");
        //UI.introPanel.selectAll("#diseaseDataLoaderTable").style("display", "");
        UI.resizePanelOnContent(UI.introPanel);
    },function(percentage){
        //UI.introPanel.select(".clusterprogressbar").select("progress").node().value = percentage*100;
    })
})

UI.interactionBar.select("#defaultEdgeButton").on("click", function(){
    var dataFileUrl;    
    switch(UI.interactionBar.select("#defaulEdgeSelect").node().value){
        case "HIPPIE":
            dataFileUrl = "/data/Defaultsets/interaction/hippie_current.txt";
            break;
        default:
            dataFileUrl = "/data/Defaultsets/interaction/hippie_current.txt";
    }
    //UI.introPanel.select(".noEdgeScoreColumn").style("display", "none");
    //UI.introPanel.select(".edgeprogressbar").style("display", "");

    var interactionDataHandler = new DataHandlers.EdgeDataHandler(dataFileUrl, "tsv", function(){
        this.columnsOfImportance.source = "SourceGeneId";
        this.columnsOfImportance.target = "TargetGeneId";
        this.columnsOfImportance.scores = ["HippieScore"];
        this.processData();
        //UI.introPanel.select(".noEdgeScoreColumn").style("display", "");
        //UI.introPanel.select(".edgeprogressbar").style("display", "none");
        //UI.introPanel.select(".noEdgeScoreColumn").text("Default interaction set chosen, no column selection");
        //UI.introPanel.select(".noEdgeScoreColumn").attr("colspan", 2);
        UI.resizePanelOnContent(UI.introPanel);
        subGeneGraph.selectedLinkScore = geneGraph.selectedLinkScore;
        UI.parameterPanel.select("#geneEdgeScoreSelection").node().value = this.columnsOfImportance.scores[0];
        UI.updateSelections("geneEdgeScore", this.columnsOfImportance.scores); 
        UI.updateStatistics();
        UI.geneSVG.node().resetVisuals();
        if(subGeneGraph.id != undefined) buildSubGeneGraph(clusterGraph.nodes[subGeneGraph.id]);
        updateAllVisuals();
        UI.resizePanelOnContent(UI.introPanel);
    },function(percentage){
        //UI.introPanel.select(".edgeprogressbar").select("progress").node().value = percentage*100;
    })
})

UI.diseaseBar.select("#defaultDiseaseButton").on("click", function(){
    var dataFileUrl;    
    switch(UI.diseaseBar.select("#defaulDiseaseSelect").node().value){
        case "GWAS":
            dataFileUrl = "/data/Defaultsets/disease/gwascatalog.txt";
            break;
        default:
            dataFileUrl = "/data/Defaultsets/disease/gwascatalog.txt";
    }
    //UI.introPanel.select(".noDiseaseColumn").style("display", "none");
    //UI.introPanel.select(".diseaseprogressbar").style("display", "");

    var diseaseDataHandler = new DataHandlers.DiseaseDataHandler(dataFileUrl, "tsv", function(){
        this.columnsOfImportance.gene = ["Mapped_gene","Reported Gene(s)"];
        this.columnsOfImportance.trait = "Disease/Trait";
        this.columnsOfImportance.score = "p-Value";
        this.processData();
        //UI.introPanel.select(".noDiseaseColumn").style("display", "");
        //UI.introPanel.select(".diseaseprogressbar").style("display", "none");
        //UI.introPanel.select(".noDiseaseColumn").text("Default interaction set chosen, no column selection");
        //UI.introPanel.select(".noDiseaseColumn").attr("colspan", 2);
        UI.resizePanelOnContent(UI.introPanel);
        UI.updateSelections("diseaseBarDisease", Object.keys(diseaseSets));
        clusterGraph.selectedNodeScore.color = Object.keys(diseaseSets)[0];
        subGeneGraph.selectedNodeScore.color = Object.keys(diseaseSets)[0];
        UI.updateStatistics();
        updateAllVisuals();
        UI.resizePanelOnContent(UI.introPanel);
    },function(percentage){
        //UI.introPanel.select(".diseaseprogressbar").select("progress").node().value = percentage*100;
    })
})

d3.select("body")
    .on("keydown", function() {
        switch(d3.event.keyCode){
            case 84: UI.textDisableButton.on("click")();
            break;
            case 69: UI.dynamicHighlightExpansionButton.on("click")();
            break;
            case 71: UI.geneHighlightButton.select("rect").on("click")();
            break;
            case 68: UI.diseaseBar.select("rect").on("click")();
            break;
            case 72: UI.helpButton.on("click")();
            break;
            case 39: nextTutSubject();
            break;
            case 37:  prevTutSubject();
            break;
            default: ;
        }

    })

d3.select("#tutStartButton").on("click", function(){
    startTutorial();
})