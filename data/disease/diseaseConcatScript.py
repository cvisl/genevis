import csv
import pdb

f = open('newConcatenatedDiseases', 'w');

with open('Alzheimer_vegasout.out', 'r') as csvfile:
    for i, line in enumerate(csvfile):
        if i != 0: 
            line = line.replace('\n', ' Alzheimer\n')
            f.write(line)
        else:
            line = line.replace('\n', ' diseases\n')
            f.write(line)

with open('Bipolar-EA_vegasout.out', 'r') as csvfile:
    for i, line in enumerate(csvfile):
        if i != 0: 
            line = line.replace('\n', ' Bipolar-EA\n')
            f.write(line)

with open('CoronaryArteryDisease-cc_vegasout.out', 'r') as csvfile:
    for i, line in enumerate(csvfile):
        if i != 0: 
            line = line.replace('\n', ' CoronaryArteryDisease-cc\n')
            f.write(line)

with open('CrohnsDisease-cc_vegasout.out', 'r') as csvfile:
    for i, line in enumerate(csvfile):
        if i != 0: 
            line = line.replace('\n', ' CrohnsDisease-cc\n')
            f.write(line)

with open('Depression_vegasout.out', 'r') as csvfile:
    for i, line in enumerate(csvfile):
        if i != 0: 
            line = line.replace('\n', ' Depression\n')
            f.write(line)

with open('Hypertension-cc_vegasout.out', 'r') as csvfile:
    for i, line in enumerate(csvfile):
        if i != 0: 
            line = line.replace('\n', ' Hypertension-cc\n')
            f.write(line)

with open('RheumatoidArthritis-cc_vegasout.out', 'r') as csvfile:
    for i, line in enumerate(csvfile):
        if i != 0: 
            line = line.replace('\n', ' RheumatoidArthritis-cc\n')
            f.write(line)