import csv
import sys

clusters = [];
genes = {};
tresh = sys.argv[1];
print tresh

with open('hippie_current.txt', 'r') as csvfile:
    csvreader = csv.reader(csvfile, delimiter='\t')
    for i, row in enumerate(csvreader):
        if i != 0:
            sourceGene = row[0]
            targetGene = row[2]
            if sourceGene in genes: genes[sourceGene][targetGene] = row[4];
            else: genes[sourceGene] = {targetGene:row[4]}
            if targetGene in genes: genes[targetGene][sourceGene] = row[4];
            else: genes[targetGene] = {sourceGene:row[4]}
    print genes