var script = document.createElement('script');
script.type= 'text/javascript';
script.src = 'http://d3js.org/d3.v3.min.js';
document.head.appendChild(script);

var doc = d3.select(window.document);

var tds = doc.selectAll("td")[0];

var clusterWeights = {"ALZ":Math.random(),"AUT":Math.random(),"EPI":Math.random(),"Tum":Math.random(),"BUH":Math.random(),"BUL":Math.random(),"BLA":Math.random(),"VLI":Math.random(),"KIL":Math.random(),"POL":Math.random()}

var protein = function(UPId, EgId, genepool, interactions){
    this.UPId = UPId;
    this.EgId = EgId;
    this.genepool = genepool;
    this.interactions = {};
    this.clusterAssociations = {"ALZ":0,"AUT":0,"EPI":0,"Tum":0,"BUH":0,"BUL":0,"BLA":0,"VLI":0,"KIL":0,"POL":0};
};

protein.prototype.setRandomClusters = function(){
    var p = this;
    Object.keys(p.clusterAssociations).forEach(function(a){
        p.clusterAssociations[a] = (Math.random()+clusterWeights[a])/2 < 0.5 ? 0 : (Math.random()+clusterWeights[a])/2;
    });
}

var proteins =  {};

proteins["HD_HUMAN"] = new protein("HD_HUMAN", 3064, "HTT", {});

for(var i = 0; i<1000; i+=5){
    if(tds[i].innerText != "HD_HUMAN"){
        
        proteins["HD_HUMAN"].interactions[tds[i].innerText] = tds[i+3].innerText;
        proteins[tds[i].innerText] = new protein(tds[i].innerText,tds[i+1].innerText,tds[i+2].innerText,{});
        proteins[tds[i].innerText].setRandomClusters();
    }
};

var kys = Object.keys(proteins.HD_HUMAN.interactions);

kys.forEach(function(d){
    kys.forEach(function(k){
        if(d != k) proteins[d].interactions[k] = "0";
        });
    d3.html("http://cbdm.mdc-berlin.de/tools/hippie/query.php?s=" + d, function(error, h) {
        var tds2 = d3.select(h).selectAll("td")[0];
        for(var i = 0; i<tds2.length; i+=5){
            if(proteins[tds2[i].innerText] != undefined){
                if(tds2[i].innerText != d){
                    
                    proteins[d].interactions[tds2[i].innerText] = tds2[i+3].innerText;
                }
            }
        }   
    });
});