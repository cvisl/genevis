var newClusterFileJsonArray = {};
var funcNames = {};
var newClusterFileTSV = "";

function makeClusterDataFile(path, delimiter){
    funcNames = {};
    newClusterFileJsonArray = {};
    var dsv = d3.dsv(delimiter, "text/plain");
    dsv(path,function(error, genes){
        genes.forEach(function(gene){
            funcNames[gene.CLUSTER]= gene.CLUSTER.replace(/[~!@$%^&*()+=,.\/';:"?><\[\]{}|`#]/g, "");
        })
        genes.forEach(function(gene){
            if(newClusterFileJsonArray[gene.GENEID] == undefined){
                var row = {};
                row.geneEntrezId = gene.GENEID;
                getGeneName(gene.GENEID, function(genename){
                    row.geneName = genename;    
                });
                Object.keys(funcNames).forEach(function(funcname){
                    row[funcNames[funcname]] = 0;
                })
                row[funcNames[gene.CLUSTER]] = 1;
                newClusterFileJsonArray[gene.GENEID] = row;
            }
            else{
                var funcNamesWithNumber = [];
                Object.keys(funcNames).forEach(function(funcname){
                    if(newClusterFileJsonArray[gene.GENEID][funcNames[funcname]] > 0) funcNamesWithNumber.push(funcNames[funcname]);
                })
                funcNamesWithNumber.forEach(function(d,i,a){
                    newClusterFileJsonArray[gene.GENEID][d] = 1/(a.length+1);
                })
                newClusterFileJsonArray[gene.GENEID][funcNames[gene.CLUSTER]] = 1/(funcNamesWithNumber.length+1);                    
            } 
        })
        /*json2csv({data: newClusterFileJsonArray, fields:Object.keys(newClusterFileJsonArray[0]), del: '\t'}, function(err, tsv) {
            if (err) console.log(err);
            fs.writeFile('newClusterFile.csv', tsv, function(err) {
                if (err) throw err;
                console.log('file saved');
            });
        });  */
    })
}

function checkGeneNameUndefined(){
    var genes = Object.keys(newClusterFileJsonArray);
    genes.forEach(function(g){
        if(newClusterFileJsonArray[g].geneName == undefined){
            getGeneName(g, function(genename){ newClusterFileJsonArray[g].geneName = genename;});
        }
    })
}

function getGeneName(geneid, callback){
    d3.html("http://www.ncbi.nlm.nih.gov/gene/?term=" + geneid, function(error, site){
        callback(site.getElementById("gene-name").getElementsByTagName("span")[0].innerHTML);
    });
}

function makeTSV(){
    var genes =Object.keys(newClusterFileJsonArray);
    var fields = Object.keys(newClusterFileJsonArray[genes[0]]);
    var link = document.createElement('a');
    newClusterFileTSV = "";
    link.text = "DOWNLOAD"
    //making header
    fields.forEach(function(f,i){
        if(i != (fields.length - 1)) newClusterFileTSV += f + "\t";
        else newClusterFileTSV += f; 
    })
    newClusterFileTSV += "\n";

    genes.forEach(function(g){
        if(newClusterFileJsonArray[g].geneName != undefined){
            fields.forEach(function(f,i){
                if(i != (fields.length - 1)) newClusterFileTSV += newClusterFileJsonArray[g][f] + "\t";
                else newClusterFileTSV += newClusterFileJsonArray[g][f];
            })
            newClusterFileTSV += "\n";
        }
    })
    link.href = makeTextFile(newClusterFileTSV);
    document.body.appendChild(link);
}

function makeTextFile(text) {
    var textFile;
    var data = new Blob([text], {type: 'text/plain'});

    // If we are replacing a previously generated file we need to
    // manually revoke the object URL to avoid memory leaks.
    if (textFile !== null) {
      window.URL.revokeObjectURL(textFile);
    }

    textFile = window.URL.createObjectURL(data);

    return textFile;
  };