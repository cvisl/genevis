// Debug hotkey
$(window).keydown(function(e) { if (e.keyCode == 123) debugger; });

// INIT GENEVIS
UI.parameterPanel.select("#dynamicLinkStrength").node().checked = true;
UI.parameterPanel.select("#linkStrength").property("disabled", true);
UI.parameterPanel.select("#linkStrength").property("readOnly", true);                
                    
var clusterGraph = new Graph.Graph(undefined, undefined, "clusterGraph");
var geneGraph = new Graph.Graph(undefined, undefined, "geneGraph");
var subGeneGraph = new Graph.Graph(undefined, undefined, "subGeneGraph");

States.initGraphVisualStates();

initGraphInfo();

UI.init();
UI.initParameters();

var diseaseSets = {};

// INIT FINISHED
function initGraphInfo(){
    //Init cluster graph
    clusterGraph.info.nodeMostGenes = {"genes":{}}; 
    clusterGraph.info.clusterJacardMaxMin = [Number.MAX_VALUE,Number.MIN_VALUE];
    clusterGraph.info.clusterAssociationTreshold = UI.parameterPanel.select("#clusterAssociationThreshold").node().value;
    clusterGraph.info.clusterColors = {};

    //Init gene cluster
    geneGraph.info.highlightThreshold = UI.parameterPanel.select("#highlightThreshold").node().value;
    geneGraph.info.highlightTop = UI.parameterPanel.select("#highlightTop").node().value;
    geneGraph.info.highlightConnectionLevel = UI.parameterPanel.select("#highlightLevelOfConnection").node().value;;
    geneGraph.info.highlightMode = UI.parameterPanel.select("#geneHighLightMode").node().value;
    geneGraph.info.expandChecked = false;
    geneGraph.info.geneEntrezLookupTable = {};
    geneGraph.info.currentQueriedGene = "";
    geneGraph.info.prevQueriedGene = "";
    geneGraph.info.edgeScoresMaxMins = {};
}

function resetScene(){
    clusterGraph = new Graph.Graph(undefined, undefined, "clusterGraph");
    geneGraph = new Graph.Graph(undefined, undefined, "geneGraph");
    subGeneGraph = new Graph.Graph(undefined, undefined, "subGeneGraph");

    initGraphInfo();

    UI.clusterSVG.selectAll(".node, .link").remove();
    UI.geneSVG.selectAll(".node, .link").remove();

    States.initGraphVisualStates();
}

function buildSubGeneGraph(clusterNode){
    subGeneGraph.clearGraph();
    if(clusterNode != undefined){
      subGeneGraph.id = clusterNode.id;
      subGeneGraph.info = geneGraph.info;
      subGeneGraph.selectedNodeScore.scale = "clusterAssociation";
      Object.keys(geneGraph.info.edgeScoresMaxMins).forEach(function(score){
        geneGraph.info.edgeScoresMaxMins[score] = [Number.MAX_VALUE,Number.MIN_VALUE]; 
      });
      Object.keys(clusterNode.data.genes).forEach(function(p){
          var geneGraphNode = geneGraph.nodes[p];
          var subGeneGraphNode = subGeneGraph.addNode(p, undefined, {"clusterAssociation": clusterNode.data.genes[p]}, {}, geneGraphNode.data);
          Object.keys(geneGraphNode.links).forEach(function(targetName){
            var link = geneGraphNode.links[targetName];
            var targetNode = subGeneGraph.nodes[targetName];
            if(targetNode != undefined){
              subGeneGraph.addLink(subGeneGraphNode, targetNode, link.scores, link.data);
              //updating minmaxes
              Object.keys(geneGraph.info.edgeScoresMaxMins).forEach(function(score){
                  if(geneGraph.info.edgeScoresMaxMins[score][0] > link.scores[score]) geneGraph.info.edgeScoresMaxMins[score][0] = link.scores[score]; 
                  if(geneGraph.info.edgeScoresMaxMins[score][1] < link.scores[score]) geneGraph.info.edgeScoresMaxMins[score][1] = link.scores[score];
              });
            } 
          });
      });
    
      Object.keys(geneGraph.info.edgeScoresMaxMins).forEach(function(score){
        if(geneGraph.info.edgeScoresMaxMins[score][0] >= 0 && geneGraph.info.edgeScoresMaxMins[score][1] <= 1) geneGraph.info.edgeScoresMaxMins[score] = [0,1]; 
      });
      UI.updateStatistics();
    }
}

function updateAllVisuals(){
  updateGraphVisuals(clusterGraph, UI.clusterSVG, clusterNodeAppender, clusterLinkAppender, clusterLinkUpdater);
  updateGraphVisuals(subGeneGraph, UI.geneSVG, geneNodeAppender, geneLinkAppender, geneLinkUpdater);
}

function sortLinks(a,b){
    var linka = a.graph.links[a.id];
    var linkb = b.graph.links[b.id];
    if(linka.scores[linka.graph.selectedLinkScore] > linkb.scores[linkb.graph.selectedLinkScore]) return 1;
    if(linka.scores[linka.graph.selectedLinkScore] < linkb.scores[linkb.graph.selectedLinkScore]) return -1;
    return 0;
}

function updateGraphVisuals(graph, svg, nodeAppender, linkAppender, linkUpdater){
    var nodeAssoArray={};
    var dataNodes = d3.values(graph.nodes).map(function(d){
     var node = {'id': d.id, 'graph':d.graph};
     nodeAssoArray[d.id] = node;
     return node;
    });
    var dataLinks = d3.values(graph.links).map(function(d){
     return {'id': d.id, 'graph':d.graph, 'source': nodeAssoArray[d.source.id], 'target': nodeAssoArray[d.target.id]};
    });
    
    graph.visualState.currentForceLayout.stop()
      .size([svg.attr("width"), svg.attr("height")])
      .nodes(dataNodes)
      .links(dataLinks)
      .startIf();
    
    var links = svg.node().drawPanel.selectAll(".link")
        .data(dataLinks.filter(function(d){
          var link = d.graph.links[d.id];
          return link.graph.linkScoreScale(link.scores[link.graph.selectedLinkScore]) != 0;  
        }), function(d){
          return "id" + d.id;
        });
        
    links.exit().remove();
    
    linkAppender(links.enter())
        .attr("class", "link");

    linkUpdater(links);
        
    links.each(function(d){
      var link = d.graph.links[d.id];
      link.element = this;
    });
    links.sort(sortLinks);
        
    var nodes = svg.node().drawPanel.selectAll(".node")
        .data(dataNodes, function(d){return d.id;});

    nodes.exit().remove();
    
    nodeAppender(nodes.enter())
      .classed("node", true)
      .call(graph.visualState.currentForceLayout.drag);        
        
    nodes.each(function(d){
      node = d.graph.nodes[d.id]
      node.element = this;
    });
        
    graph.visualState.currentForceLayout.drag()
        .on("dragstart", dragstart)
        .on("drag", dragging)
        .on("dragend", dragend);

    graph.visualState.currentForceLayout.on("tick", tickDraw.bind(graph));

    function tickDraw(){
      //if(this.visualState.currentForceLayout.alpha() < 0.05 || this.id != "clusterGraph"){
        links.attr("x1", function(d) { return ~~svg.node().zoomMappingX(d.source.x); })
          .attr("y1", function(d) { return ~~svg.node().zoomMappingY(d.source.y); })
          .attr("x2", function(d) { return ~~svg.node().zoomMappingX(d.target.x); })
          .attr("y2", function(d) { return ~~svg.node().zoomMappingY(d.target.y); });

        nodes.attr("transform", function(d){ return "translate(" + ~~svg.node().zoomMappingX(d.x) + "," + ~~svg.node().zoomMappingY(d.y) + ")";});
      //}
    }

    geneQuery();
   if(UI.diseaseButton.classed("button on")) highlightClusters(Object.keys(diseaseSets[clusterGraph.selectedNodeScore.color].clusters));
}

function clusterNodeAppender(selection){
  selection = selection.append("g")
            .classed("cluster", true)
            .attr("id", function(d){return "id"+d.id;})
            .on("click", function(d){clickClusterNode(d.graph.nodes[d.id]);});

  selection.append("ellipse")
            .style("fill", function(d){ 
              return clusterGraph.visualState.currentNodeColorMap(d.graph.nodes[d.id]);
            })
            .attr("ry", function(d){
             return ~~Math.sqrt(clusterGraph.visualState.currentNodeScaleMap.ellipseSizeScale(Object.keys(d.graph.nodes[d.id].data.genes).length/Object.keys(clusterGraph.info.nodeMostGenes.data.genes).length));
           })
            .attr("rx", function(d){ 
              var node = d.graph.nodes[d.id];
              return clusterGraph.visualState.currentNodeScaleMap.ellipseMajorAxisScale(Object.keys(node.data.genes).length/Object.keys(clusterGraph.info.nodeMostGenes.data.genes).length,node.scores[node.graph.selectedNodeScore.scale.ellipseAxisScore]);
            });

    selection.append("text")
        .classed("clusterText", true)
        .attr("text-anchor", "middle")
        .attr("dy", function(d){
          return ~~(-Math.sqrt(clusterGraph.visualState.currentNodeScaleMap.ellipseSizeScale(Object.keys(d.graph.nodes[d.id].data.genes).length/Object.keys(clusterGraph.info.nodeMostGenes.data.genes).length)) - 5);
        })
        .text(function(d){ return d.id; });

  return selection;
}

function geneNodeAppender(selection){

  var pie = d3.layout.pie()
    .value(function(d) { 
        return d.association; 
        })
  .sort(function(a,b){    
    if(a.association < b.association) return 1;
    if(a.association > b.association) return -1;
    return 0;
  });
        
  var arc = d3.svg.arc()
    .outerRadius(function(){
        var node = this.parentElement.__data__.graph.nodes[this.parentElement.__data__.id]
        return ~~Math.sqrt(subGeneGraph.visualState.currentNodeScaleMap(node.scores[node.graph.selectedNodeScore.scale]));
    })
    .innerRadius(0);

  selection = selection.append("g")
    .classed("gene", true)
    .attr("id", function(d){return "id"+d.id;})
    .on("mouseenter", function(d){
                var node = d.graph.nodes[d.id];
                unhighlightConnectedLinks();
                highlightGenes(node);
                growNode.call(this, node);
              })
    .on("mouseleave", function(d){
                unhighlightConnectedLinks();
                var gene = subGeneGraph.nodes[geneGraph.info.currentQueriedGene];
                if(gene != undefined && UI.geneHighlightButton.classed("button on")) highlightGenes(gene);
                shrinkNode.call(this, d.graph.nodes[d.id]);
              })
    .on("click", function(d){clickGeneNode(d.graph.nodes[d.id]);});
  
    selection.append("text")
        .classed("geneText", true)
        .attr("dx", function(d){
          var node = d.graph.nodes[d.id];
            return 5+~~Math.sqrt(subGeneGraph.visualState.currentNodeScaleMap(node.scores[node.graph.selectedNodeScore.scale]));
        })
        .attr("dy", "0.35em")
        .text(function(d){ return d.id; });

  var piepiece = selection.selectAll(".pie")
    .data(function(d){
      return pie(d.graph.nodes[d.id].data.clusters);
      })
      .enter().append("path")
        .attr("class", "pie")
        .attr("d", arc)
        .style("fill", function(d){ 
          return subGeneGraph.visualState.currentNodeColorMap.call(this, d.data.cluster);
        })
        .on("mouseenter", function(d){
            undoClusterHighlight();
            highlightClusters([d.data.cluster]);
        })
        .on("mouseleave", function(d){
            undoClusterHighlight();
            if(UI.diseaseButton.classed("button on")) highlightClusters(Object.keys(diseaseSets[clusterGraph.selectedNodeScore.color].clusters));
            else if(geneGraph.info.currentQueriedGene != undefined && UI.geneHighlightButton.classed("button on")) highlightClusters(geneGraph.nodes[geneGraph.info.currentQueriedGene].data.clusters.map(function(d){ return d.cluster;}));
        })
        .on("click", clickGeneNodePiePiece);     

  piepiece.append("title")
        .text(function(d) {
            var graphnode = this.parentElement.parentElement.__data__.graph.nodes[this.parentElement.parentElement.__data__.id];
            return "Gene name: " + graphnode.id + "\n" +
                   "Graph cluster " + graphnode.graph.id +" association: "+ (100*graphnode.scores.clusterAssociation).toFixed(2) + "%\n" +
                   "Pie cluster " + d.data.cluster + " association: " + (100*d.data.association).toFixed(2) + "%\n"; 
        });

  function growNode(d){
    var geneNodeSelection = d3.select(this);
    d3.selectAll(".gene").sort(function(a,b){ //SLOW
      if(a.id != d.id) return -1;
      else return 1;
    });
    //this.parentElement.appendChild(this); //NEED TO FIX THIS FOR FIREFOX
    geneNodeSelection
      .selectAll("path")
        .transition().duration(500).attrTween("d", function(a) {
           var i = d3.interpolate(this.__data__, a),
              k = d3.interpolate(arc.outerRadius().call(this), ~~Math.sqrt(subGeneGraph.visualState.currentNodeScaleMap(3)));
           return function(t) {
               return arc.outerRadius(k(t)).call(this, i(t));
           };
      });
    geneNodeSelection
      .select("text")
        .transition()
        .duration(500)
            .attr("dx", 5 + ~~Math.sqrt(subGeneGraph.visualState.currentNodeScaleMap(3)));                                
  }

  function shrinkNode(){
     d3.select(this)
      .selectAll("path")
        .transition().duration(500).attrTween("d", function(a) {
           var i = d3.interpolate(this.__data__, a);
              var node = this.parentElement.__data__.graph.nodes[this.parentElement.__data__.id];
              var k = d3.interpolate(arc.outerRadius().call(this), ~~Math.sqrt(subGeneGraph.visualState.currentNodeScaleMap(node.scores[node.graph.selectedNodeScore.scale])));
           return function(t) {
               return arc.outerRadius(k(t)).call(this, i(t));
           };
      });
      d3.select(this)
        .select("text")
          .transition()
          .duration(500)
            .attr("dx", function(d){
              var node = d.graph.nodes[d.id];
              return 5 + ~~Math.sqrt(subGeneGraph.visualState.currentNodeScaleMap(node.scores[node.graph.selectedNodeScore.scale]));
            });
  }

  return selection;
}

function geneLinkAppender(selection){
  return selection.insert("line", ".node")
          .attr("id", function(d){return d.id;})
          .style("stroke", function(d) { 
            var link = d.graph.links[d.id];
            return subGeneGraph.visualState.currentLinkColorMap(link.graph.linkScoreScale(link.scores[link.graph.selectedLinkScore])); 
            //return "#018571";
          })
          .style("stroke-width", function(d){ 
            var link = d.graph.links[d.id];
            return subGeneGraph.visualState.currentLinkScaleMap(link.graph.linkScoreScale(link.scores[link.graph.selectedLinkScore]));
            //return 2;
          });
}

function geneNodeUpdater(selection){
    return selection
        .selectAll(".pie")
            .style("fill", function(d){
                return subGeneGraph.visualState.currentNodeColorMap.call(this, d.data.cluster);
            });
}

function geneLinkUpdater(selection){
  return selection
          .style("stroke", function(d) { 
            var link = d.graph.links[d.id];
            return subGeneGraph.visualState.currentLinkColorMap(link.graph.linkScoreScale(link.scores[link.graph.selectedLinkScore])); 
          })
          .style("stroke-width", function(d){ 
            var link = d.graph.links[d.id];
            return subGeneGraph.visualState.currentLinkScaleMap(link.graph.linkScoreScale(link.scores[link.graph.selectedLinkScore]));
          });
}

function clusterLinkAppender(selection){
    return selection.insert("line", ".node")
          .attr("id", function(d){return "id"+d.id;})
          .style("stroke", function(d) { 
            var link = d.graph.links[d.id];
            return clusterGraph.visualState.currentLinkColorMap(link.graph.linkScoreScale(link.scores[link.graph.selectedLinkScore])); 
          })
          .style("stroke-width", function(d){ 
            var link = d.graph.links[d.id];
            return clusterGraph.visualState.currentLinkScaleMap(link.graph.linkScoreScale(link.scores[link.graph.selectedLinkScore]));
          });
}

function clusterNodeUpdater(selection){
    undoClusterHighlight();
    selection
        .select("ellipse")
            .style("fill", function(d){ 
                  return clusterGraph.visualState.currentNodeColorMap(d.graph.nodes[d.id]);
                });
    if(UI.diseaseButton.classed("button on")) highlightClusters(Object.keys(diseaseSets[clusterGraph.selectedNodeScore.color].clusters)); //<=== Hackerdihack
    else undoClusterHighlight(); //<=== Hackerdihack
    return selection;
}

function clusterLinkUpdater(selection){
    return selection
            .style("stroke", function(d) { 
              var link = d.graph.links[d.id];
              return clusterGraph.visualState.currentLinkColorMap(link.graph.linkScoreScale(link.scores[link.graph.selectedLinkScore])); 
            })
            .style("stroke-width", function(d){ 
              var link = d.graph.links[d.id];
              return clusterGraph.visualState.currentLinkScaleMap(link.graph.linkScoreScale(link.scores[link.graph.selectedLinkScore]));
            });
}

function clickClusterNode(cluster){
  if (d3.event.defaultPrevented) return; // click suppressed
  d3.event.stopPropagation();
  UI.geneSVG.node().resetVisuals();
  buildSubGeneGraph(cluster);
  if(subGeneGraph.selectedLinkScore != undefined) subGeneGraph.linkScoreScale.domain(geneGraph.info.edgeScoresMaxMins[subGeneGraph.selectedLinkScore]);
  UI.geneSVG.select(".panelLabel").text("Gene View of cluster "+cluster.id);
  if(UI.geneSVG.style("display") == "none") UI.focusSecondary();
  else updateGraphVisuals(subGeneGraph, UI.geneSVG, geneNodeAppender, geneLinkAppender, geneLinkUpdater);

}

function dragstart(){
    d3.event.sourceEvent.stopPropagation();
}

function dragging(){
    d3.select(this).attr("transform", function(d){
        d.x = this.farthestViewportElement.zoomMappingX.invert(d3.event.sourceEvent.x-parseInt(this.farthestViewportElement.getBoundingClientRect().left));
        d.y = this.farthestViewportElement.zoomMappingY.invert(d3.event.sourceEvent.y-parseInt(this.farthestViewportElement.getBoundingClientRect().top));
        d.px = d.x; // cancel velocity
        d.py = d.y; // cancel velocity
        return "translate(" + this.farthestViewportElement.zoomMappingX(d.x) + "," + this.farthestViewportElement.zoomMappingY(d.y) + ")";
    });
}

function dragend(d){
}

function clickGeneNode(d){
  if(d3.event.defaultPrevented) return; // click suppressed
  //birdNestView.call(this);
  geneGraph.info.currentQueriedGene = d.id;
  UI.activateGeneQueryMode();
}

function clickGeneNodePiePiece(d){
  if(d3.event.defaultPrevented) return; // click suppressed
  if(d3.event.ctrlKey){
    d3.event.stopPropagation();
    clickClusterNode(clusterGraph.nodes[d.data.cluster]);
  } 
}

function highlightGeneNode(node){
  UI.geneSVG.select("#"+node.id)                               
    .classed("highlightedNode", true)
    .classed("faded", false);
}

function highlightGeneLink(link){
  UI.geneSVG.select("#"+link.id)                 
    .classed("highlightedLink", true)
    .classed("faded", false)
    .style("stroke",  "#ef3d47");
}

function fadeNonHighlightedGenes(){
    UI.geneSVG.selectAll(".node")
      .filter(function(){
         return !this.classList.contains("highlightedNode");
      })
      .classed("faded", true);
      
    UI.geneSVG.selectAll(".link")
      .filter(function(){
         return !this.classList.contains("highlightedLink");
      })
      .classed("faded", true);
}

function highlightGenes(gene){
    var geneInSubGeneGraph = subGeneGraph.nodes[gene.id];
    if(geneInSubGeneGraph != undefined){
        switch(geneGraph.info.highlightMode){
            case "threshold": highlightByThreshold(geneInSubGeneGraph);
            break;
            case "top": highlightByTopLinks(geneInSubGeneGraph);
            break;
            case "lvlOfConnection": highlightByLevelOfConnection(geneInSubGeneGraph);
            break;
            default: highlightByThreshold(geneInSubGeneGraph);
        }
    }
}

function highlightByThreshold(highlightNode){
    var searchNodes = {};
    searchNodes[highlightNode.id] = highlightNode;
    var searching = true;
    while(searching){
        temp = {};
        Object.keys(searchNodes).forEach(function(n){
            var node = searchNodes[n];
            highlightGeneNode(node.element);
            Object.keys(node.links).forEach(function(l){
                newLink = node.links[l];
                if(newLink.element != undefined && newLink.graph.linkScoreScale(newLink.scores[newLink.graph.selectedLinkScore]) > geneGraph.info.highlightThreshold && !newLink.element.classList.contains("highlightedLink")){
                    highlightGeneLink(newLink.element);                       //same issue here
                    var newSearchNode = (newLink.source == node) ? newLink.target : newLink.source;
                    if(searchNodes[newSearchNode.id] == undefined) temp[newSearchNode.id] = newSearchNode;
                }
            });
        });
        if(Object.keys(temp).length == 0) searching = false;
        else searchNodes = temp;
    }
    fadeNonHighlightedGenes();
}

function highlightByTopLinks(highlightNode){
    var searchedNodes = {};
    var searchLinks = [];
    var topLinks = geneGraph.info.highlightTop;
    highlightGeneNode(highlightNode.element);
    Object.keys(highlightNode.links).forEach(function(l){
      searchLinks.push(highlightNode.links[l]);
    });
    searchLinks.sort(sortLinks);
    while(topLinks > 0 && searchLinks.length != 0){                      
        var newLink = searchLinks.pop();
        if(newLink.element != undefined && newLink.graph.linkScoreScale(newLink.scores[newLink.graph.selectedLinkScore]) != 0 && !newLink.element.classList.contains("highlightedLink")){
            topLinks--;
            highlightGeneLink(newLink.element);
            var newSourceNode = newLink.source;
            var newTargetNode = newLink.target;
            if(searchedNodes[newSourceNode.id] == undefined){
                searchedNodes[newSourceNode.id] = newSourceNode;
                highlightGeneNode(newSourceNode.element);
                Object.keys(newSourceNode.links).forEach(function(l){
                  if(newSourceNode.links[l].element != undefined && !newSourceNode.links[l].element.classList.contains("highlightedLink")) searchLinks.push(newSourceNode.links[l]);
                });
            } 
            if(searchedNodes[newTargetNode.id] == undefined){
                searchedNodes[newTargetNode.id] = newTargetNode;
                highlightGeneNode(newTargetNode.element);
                Object.keys(newTargetNode.links).forEach(function(l){
                  if(newTargetNode.links[l].element != undefined && !newTargetNode.links[l].element.classList.contains("highlightedLink")) searchLinks.push(newTargetNode.links[l]);
                });
            } 
            searchLinks.sort(sortLinks);
        }
    }
    fadeNonHighlightedGenes();
}

function highlightByLevelOfConnection(highlightNode){

    var nextLevelNodes = [highlightNode];
    var currentLevelNodes = [highlightNode];
    highlightGeneNode(highlightNode.element);
    var searchedNodes = {};
    var connectionLevel = geneGraph.info.highlightConnectionLevel;
    for(var i = 0; i<connectionLevel; i++){
        currentLevelNodes = nextLevelNodes;
        nextLevelNodes = [];
        currentLevelNodes.forEach(function(node){
            Object.keys(node.links).forEach(function(l){
                var link = node.links[l];
                var nextLevelNode = link.source.id == node.id ? link.target : link.source;
                if(searchedNodes[nextLevelNode.id] == undefined && link.element != undefined){
                    highlightGeneLink(link.element);
                    highlightGeneNode(nextLevelNode.element);
                    nextLevelNodes.push(nextLevelNode); 
                    searchedNodes[nextLevelNode.id] = nextLevelNode;
                } 
            });
        });
    }

    fadeNonHighlightedGenes();
}

function unhighlightConnectedLinks(){
    UI.geneSVG.selectAll(".link")
      .classed("highlightedLink", false)
      .classed("faded", false)
      .style("stroke",  function(d) { 
            var link = d.graph.links[d.id];
            return subGeneGraph.visualState.currentLinkColorMap(link.graph.linkScoreScale(link.scores[link.graph.selectedLinkScore])); 
        });

    UI.geneSVG.selectAll(".node")
      .classed("highlightedNode", false)
      .classed("faded", false);
}

function birdNestView(){
    highlightedNodes = UI.geneSVG.selectAll(".highlightedNode");
    highlightedLinks = UI.geneSVG.selectAll(".highlightedLink");
    if(!highlightedNodes.empty()){
        var linkData = highlightedLinks
          .classed("expandedLink", true)
          .classed("highlightedLink", true)
          .classed("faded", false)
          .data().map(function(d){return d.id;});
        var nodeData = highlightedNodes
          .classed("expandedNode", true)
          .classed("highlightedNode", true)
          .classed("faded", false)
          .data().map(function(d){return d.id;});
        subGeneGraph.visualState.currentForceLayout           
            .charge(subGeneGraph.visualState.currentForceLayout.highlightCharge(nodeData)) // pass node data
            .linkStrength(subGeneGraph.visualState.currentForceLayout.highlightLinkStrength(linkData)) // pass link data
            .startIf();
    }
}

function unbirdNestView(){
    expandedNodes = UI.geneSVG.selectAll(".expandedNode");
    expandedLinks = UI.geneSVG.selectAll(".expandedLink");
    if(!expandedNodes.empty()){
        expandedNodes
            .classed("expandedNode", false);
        expandedLinks
            .classed("expandedLink", false);
        resetForceState(subGeneGraph);
    }
}

function resetForceState(graph){
    graph.visualState.currentForceLayout         
      .charge(graph.visualState.currentForceLayout.selectedCharge)
      .linkStrength(graph.visualState.currentForceLayout.selectedLinkStrength)
      .startIf();
}

function geneQuery(){
    var queriedGene = geneGraph.nodes[geneGraph.info.currentQueriedGene];
    if(queriedGene != undefined && UI.geneHighlightButton.classed("button on")){
            undoClusterHighlight();
            unhighlightQueriedGenes();
            highlightQueriedGene(queriedGene);
            highlightClusters(queriedGene.data.clusters.map(function(d){ return d.cluster;}));
            unbirdNestView();
            unhighlightConnectedLinks();
            highlightGenes(queriedGene);
            if(geneGraph.info.expandChecked) birdNestView();
    }
    else{
            undoClusterHighlight();
            unhighlightQueriedGenes();
            unhighlightConnectedLinks();
            if(geneGraph.info.expandChecked) unbirdNestView();
    }
}

function highlightQueriedGene(gene){
    UI.geneSVG.select("#id"+gene.id)
        .classed("queried", true);
}

function unhighlightQueriedGenes(){
    UI.geneSVG.selectAll(".queried")
        .classed("queried", false);
}

UI.geneSVG.node().resetVisuals = function(){
    resetForceState(subGeneGraph);

    UI.geneSVG.selectAll(".node")
        .attr("class", "node");

    UI.geneSVG.selectAll(".link")
        .attr("class", "link");
}

//Cluster graph highlighting

function highlightClusters(clusters){
    clusters.forEach(function(source){

        var c = d3.select("#id"+source)
          .classed("highlightedCluster", true)
          
        var clusterNode = c.node();
        clusterNode.parentNode.appendChild(clusterNode);

        clusters.forEach(function(target){
          var link = clusterGraph.getLinkBySourceTarget(clusterGraph.nodes[source], clusterGraph.nodes[target]);
          if(link != undefined){ 
              UI.clusterSVG.select("#id" + link.source.id + link.target.id)
              .style("stroke", function(d) { 
                var link = d.graph.links[d.id];
                return clusterGraph.visualState.currentLinkColorMap(link.graph.linkScoreScale(link.scores[link.graph.selectedLinkScore])); 
              })
              .classed("highlightedClusterLink", true);
            }
        })
    })

  UI.clusterSVG.selectAll(".node")
    .filter(function(){
      return !this.classList.contains("highlightedCluster");
    })
    .classed("faded", true);

  UI.clusterSVG.selectAll(".link")
    .filter(function(){
      return !this.classList.contains("highlightedClusterLink");
    })
    .classed("faded", true)
    .style("stroke", function(d) { 
      var link = d.graph.links[d.id];
      var currentState = clusterGraph.visualState.getCurrentVisualState();
      clusterGraph.visualState.setCurrentVisualState('default');
      var defaultColor = clusterGraph.visualState.currentLinkColorMap(link.graph.linkScoreScale(link.scores[link.graph.selectedLinkScore]));
      clusterGraph.visualState.setCurrentVisualState(currentState);
      return defaultColor; 
    })
}

function undoClusterHighlight(d){
  UI.clusterSVG.selectAll(".node")
    .attr("class", "node");

  UI.clusterSVG.selectAll(".link")
    .attr("class", "link")
    .style("stroke", function(d) { 
          var link = d.graph.links[d.id];
          return clusterGraph.visualState.currentLinkColorMap(link.graph.linkScoreScale(link.scores[link.graph.selectedLinkScore])); 
        });
}

function freezeForceLayout(graph, freeze){
    graph.visualState.currentForceLayout.freeze(freeze);
}