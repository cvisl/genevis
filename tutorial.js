currentSubject = 0;
tutBalloon = d3.select("#tutBalloon");

tutSubjects = [
            {
                name:"clusterBarDataLoad", 
                pointToId:"#defaultClusterSelect", 
                text:"First off you need to load the <b>gene cluster dataset</b>. By default there are the two public datasets from KEGG and Reactome.<br><br>Once loaded a network of clusters will appear in the cluster network view and from here on you can already start to explore the cluster network. But first lets load the other datasets with the following tutorial steps to get it the most out of GeneVis.", 
                pointerDirection:"bottom",
                pointToBar: true,
            },
            {
                name:"interactionBarDataLoad", 
                pointToId:"#defaulEdgeSelect", 
                text:"The second dataset that needs to be loaded is the <b>gene interaction dataset</b>. The default dataset for the gene interactions is the HIPPIE dataset. This dataset will contain all the gene interactions that will be visible in the Gene View.", 
                pointerDirection:"bottom",
                pointToBar: true,
            },
            {
                name:"diseaseBarLoadData", 
                pointToId:"#defaulDiseaseSelect", 
                text:"The last dataset that needs to be loaded is the <b>disease association dataset</b>. The default dataset for the disease data is the GWAS dataset. This dataset contains all the gene-to-disease associations which are needed to activate the disease mode, which will be explained later in the tutorial.", 
                pointerDirection:"bottom",
                pointToBar: true,
            },
            {
                name:"clusternetworkview", 
                pointToId:"#clusterSVG", 
                text:"Now that all the datasets are loaded we can start exploring the gene clusters in the <b>cluster network view</b>. In this cluster network view each <b>node</b> is a <b>cluster</b> and each <b>edge</b> is a indication that the two connected clusters have a <b>set of overlapping genes</b>. The nodes overall size indicates the number of genes within the cluster and the \"ellipseness\" of the node represents the average gene association of the cluster. The edges width indicates the amount of overlap between the clusters.", 
                pointerDirection:"none",
                pointToBar: false,
            },
            {
                name:"clusternode", 
                pointToId:"#clusterSVG", 
                text:"Now lets open the Gene View by <b>clicking</b> on one of the <b>cluster nodes</b>.", 
                pointerDirection:"none",
                pointToBar: false,
            },
            {
                name:"genenetworkview", 
                pointToId:"#geneSVG",
                text:"The <b>Gene View</b> reveals all the genes present in the selected cluster. In this Gene View each <b>node</b> represents a <b>gene</b> and each <b>edge</b> a <b>gene interaction</b>. The size of the gene nodes represents the association with the cluster and the pie chart the distribution of this gene among other clusters and this one. The size of the edge represents the gene interaction probability." ,
                pointerDirection:"none",
                pointToBar: false,
            },
            {
                name:"genenode", 
                pointToId:"#geneSVG", 
                text:"The gene nodes have two ways of interaction.<br><br> <b>Hovering</b> over a gene will <b>highlight all the connected genes</b> based on the highlighting setting (for more information see manual).<br><br> <b>Clicking</b> a gene will <b>select or de-select the gene</b> which will hold the highlighting of connected genes, reveal all associated clusters in the cluster network view and it will open the infopanel.", 
                pointerDirection:"none",
                pointToBar: false,
            },
            {
                name:"clusterBarQueryGene", 
                pointToId:"#buttonGeneQuery", 
                text:"Besides clicking on a gene node, a gene can also be <b>queried</b> with this auto completed selection box to find your desired gene.", 
                pointerDirection:"bottom",
                pointToBar: true,
            },
            {
                name:"clusterBarLabels", 
                pointToId:"#textDisableButton", 
                text:"Whenever the number of clusters gets overwhelming and the labels of the clusters clutter the view of the network, you can <b>hide the cluster labels</b> with this button.", 
                pointerDirection:"bottom",
                pointToBar: true,
            },
            {
                name:"clusterBarDisableClearQuery", 
                pointToId:"#geneHighlightButton", 
                text:"A selected gene can be <b>de-selected</b> with this button.", 
                pointerDirection:"bottom",
                pointToBar: true,
            },
            {
                name:"interactionBarDHE", 
                pointToId:"#dynamicHighlightExpansionButton", 
                text:"<b>Dynamic highlight expansion</b> expands the highlighted genes and relieves the other genes to get a <b>better view</b> on how these genes are connected.", 
                pointerDirection:"bottom",
                pointToBar: true,
            },
            {
                name:"diseaseBarEnableDiseaseMode", 
                pointToId:"#diseaseButton", 
                text:"This button activates the <b>disease mode</b>. This disease mode <b>color codes the clusters and genes</b> in both views based on their <b>association with the selected disease</b> from the selected disease association dataset.", 
                pointerDirection:"bottom",
                pointToBar: true,
            },
            {
                name:"diseaseBarDiseaseSelection", 
                pointToId:"#diseaseBarDiseaseSelection", 
                text:"With this selection box you can <b>select one of the diseases</b> in the loaded disease dataset to be used for the disease mode.", 
                pointerDirection:"bottom",
                pointToBar: true,
            },
            {
                name:"helpButton", 
                pointToId:"#helpButton", 
                text:"This will open the <b>intropanel</b> again in which you will find a link to the manual or start this tutorial again.", 
                pointerDirection:"bottom",
                pointToBar: false,
            },
            {
                name:"infoPanel", 
                pointToId:"#infoPanelSVG", 
                text:"This button will open the <b>info panel</b>. This info panel contains <b>additional information about the selected gene</b>.", 
                pointerDirection:"top",
                pointToBar: false,
            },
            {
                name:"settingsPanel", 
                pointToId:"#primaryParameterSVG", 
                text:"This button will open the <b>settings panel</b>. In the settings panel you can find <b>additional advanced options and features</b>. For more information on these settings we must refer you to <a href=\"info.html\" target=\"_blank\">this manual</a>.", 
                pointerDirection:"top",
                pointToBar: false,
            },
            {
                name:"end",
                pointToId:"body",
                text:"This was the tutorial and if you want to know more about this visualization tool feel free to look at <a href=\"info.html\" target=\"_blank\">this manual</a>.",
                pointerDirection:"none",
                pointToBar: false,
            }
]

function startTutorial(){
    currentSubject = -1;
    UI.introPanelSVG.style("display", "none");
    tutBalloon.style("display", "inline-block");
    d3.select("#tutBackButton").style("display", "none");
    d3.select("#tutNextButton").on("click", function(){ nextTutSubject(); });
    d3.select("#tutNextButton").text("Next");
    nextTutSubject();
}

function stopTutorial(){
    currentSubject = -1;
    nextTutSubject();
    tutBalloon.style("display", "none");
}

function nextTutSubject(){
    currentSubject++;
    if(currentSubject >= tutSubjects.length - 1){
        currentSubject = tutSubjects.length - 1;
        d3.select("#tutNextButton").on("click", function(){ stopTutorial(); })
        d3.select("#tutNextButton").text("Close");
    }
    else{
        d3.select("#tutNextButton").style("display", "inline-block");
        if(currentSubject > 0) d3.select("#tutBackButton").style("display", "inline-block");
    }
    loadTutText();
    moveTutBalloon();
}

// function getElementPos(){
//     var elementPos = {right:0,left:0,top:0,bottom:0};
//     var boundingrect = d3.select("#"+tutSubjects[currentSubject].pointToId).node().getBoundingClientRect();
//     elementPos.right = windowWidth - boundingrect.right;
//     elementPos.bottom = windowHeight - boundingrect.bottom;
//     elementPos.top = boundingrect.top;
//     elementPos.left = boundingrect.left;
//     elementPos.width = boundingrect.width;
//     elementPos.height = boundingrect.height;
//     return elementPos;
// }

function prevTutSubject(){
    currentSubject--;
    if(currentSubject < 0){
        currentSubject = 0;
        d3.select("#tutBackButton").style("display", "none");
    }
    else{
        d3.select("#tutBackButton").style("display", "inline-block");
        if(currentSubject < tutSubjects.length) d3.select("#tutNextButton").style("display", "inline-block");
    }
    loadTutText();
    moveTutBalloon();
}

function loadTutText(){
    var text = tutSubjects[currentSubject].text;
    d3.select("#tutText").html(text);
}

function moveTutBalloon(){
    var subject = d3.select(tutSubjects[currentSubject].pointToId).node();
    var subjectPos = subject.getBoundingClientRect();
    var pointToBar = tutSubjects[currentSubject].pointToBar;
    var barPos = pointToBar ? subject.parentElement.parentElement.getBoundingClientRect() : undefined;
    var subjectPosMiddle = {"x": subjectPos.left + (subjectPos.width/2), "y": subjectPos.top + (subjectPos.height/2)};
    if(tutSubjects[currentSubject].pointToId == "body") subjectPosMiddle = {"x": (windowWidth/2), "y": (windowHeight/2)}; // <=== dirty hack to body height differently

    switch(tutSubjects[currentSubject].pointerDirection){
        case "bottom":
            if(((subjectPosMiddle.x-30)+(tutBalloon.style("max-width").match(/%/) == null ? parseInt(tutBalloon.style("max-width")) : (parseInt(tutBalloon.style("max-width"))/100)*windowWidth))<windowWidth){
                tutBalloon.attr("class", "tutBalloonLeft");
                tutBalloon.style("right", "auto");
                tutBalloon.style("left", (subjectPosMiddle.x-30) + "px");
            }
            else{
              tutBalloon.attr("class", "tutBalloonRight");
              tutBalloon.style("left", "auto");
              tutBalloon.style("right", ((windowWidth-subjectPosMiddle.x)-30)<0?0:((windowWidth-subjectPosMiddle.x)-30) + "px");  
            }
            tutBalloon.style("top", "auto");
            if(pointToBar) tutBalloon.style("bottom", (windowHeight-(barPos.top-20)) + "px");
            else tutBalloon.style("bottom", (windowHeight-(subjectPos.top-20)) + "px");
        break;
        case "top":
            if(((subjectPosMiddle.x-30)+(tutBalloon.style("max-width").match(/%/) == null ? parseInt(tutBalloon.style("max-width")) : (parseInt(tutBalloon.style("max-width"))/100)*windowWidth))<windowWidth){
                tutBalloon.attr("class", "tutBalloonTopLeft");
                tutBalloon.style("right", "auto");
                tutBalloon.style("left", (subjectPosMiddle.x-30) + "px");
            }
            else{
              tutBalloon.attr("class", "tutBalloonTopRight");
              tutBalloon.style("left", "auto");
              tutBalloon.style("right", ((windowWidth-subjectPosMiddle.x)-30)<0?0:((windowWidth-subjectPosMiddle.x)-30) + "px");  
            }
            tutBalloon.style("bottom", "auto");
            if(pointToBar) tutBalloon.style("top", (barPos.bottom+20) + "px");
            else tutBalloon.style("top", (subjectPos.bottom+20) + "px");
        break;
        case "none":
            tutBalloon.attr("class", "tutBalloon");
            tutBalloon.style("bottom", "auto");
            tutBalloon.style("left",  (subjectPosMiddle.x - (parseInt(tutBalloon.style("width"))/2)) + "px");
            tutBalloon.style("top",  (subjectPosMiddle.y - (parseInt(tutBalloon.style("height"))/2)) + "px");
        break;
        default:
    }
}

d3.select("#tutBackButton").on("click", function(){
    prevTutSubject();	
})

